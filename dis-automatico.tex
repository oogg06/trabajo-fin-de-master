\section{Diseño automático de juegos de instrucciones}
El diseño automático de juegos de instrucciones ya ha sido estudiado en más de una ocasión. En este sentido \cite{tesis} ya demuestra la utilidad del uso de algoritmos en el diseño de un microprocesador especializado en la ejecución de Prolog utilizando como entradas la descripción de una ruta de datos.

En la actualidad el diseño automático de juegos de instrucciones se estudia como una aplicación específica de ciertos microprocesadores llamados ASIPs (Application-Specific Instruction Processor). Estos dispositivos funcionan como microprocesadores normales con una salvedad: permiten la incorporación de instrucciones nuevas. Tales instrucciones permiten a los diseñadores de SoC's estudiar primero los tipos de programas que van a ejecutar y si observan que determinadas operaciones son muy comunes, reprogramar los ASIP para que las incorporen. De esta forma, se obtienen ganancias de rendimiento apreciables al trasladar el trabajo del software al hardware.

{Aunque el hardware físico y el hardware lógico no sean elementos directamente comparables, se pueden aplicar algunos de los principios para intentar obtener de forma automática juegos de instrucciones para máquinas virtuales que permitan obtener valores óptimos de determinadas métricas}

A continuación se expone un pequeño estudio bibliográfico examinando los diversos métodos propuestos para diseñar un juego de instrucciones.

\subsection{Diseño manual}
En \cite{tesis} se clasifican los métodos manuales para el diseño de instrucciones de acuerdo a diversos parámetros:

\begin{table}[h]
\centering
\begin{tabular}{|c|c|}
\hline
\parbox{5cm}{En función de como se generan} & Iterativos vs Constructivos \\
\hline
\parbox{5cm}{Según la unidad usada} & Instrucción vs Característica \\
\hline
\parbox{5cm}{Métrica usada} & \parbox{5cm}{Recuento de ciclos, tamaño del código, tamaño del conjunto de instrucciones, coste/valor} \\
\hline
\end{tabular}
\end{table}

En los métodos iterativos se parte de un conjunto de instrucciones inicial que se va refinando por medio de transformaciones. Así, el conjunto de instrucciones inicial puede provenir de sitios muy diversos como un lenguaje de alto nivel o algún conjunto de instrucciones ya conocido. Las transformaciones pueden consistir en añadir, quitar o reemplazar unas instrucciones por otras

En los métodos constructivos no hay un proceso sino simplemente una determinación de un conjunto de instrucciones final, normalmente obtenido a partir de la experiencia de los diseñadores de microprocesadores.

Evidentemente, no siempre se diseñará un juego de instrucciones basándose solamente en operaciones en sí, sino que los arquitectos del hardware pueden estar más interesados en ofrecer características concretas a los programadores. Por último, todos estos métodos pueden utilizar diversas métricas para determinar la idoneidad del conjunto de instrucciones.

{Un objetivo de este trabajo de fin de máster es presentar la posibilidad de diseñar automáticamente juegos de instrucciones que se adapten a diversas métricas, es decir parametrizable.}

Puede obtenerse mucha más información sobre los métodos manuales para el diseño de instrucciones en la bibliografía propuesta~\cite{tesis}.

\subsection{Grafos SSA}
Los grafos SSA o {\it Single Static Assignment} son grafos acíclicos dirigidos que intentan plasmar una operación de alto nivel en forma de pequeñas operaciones básicas con reglas muy claras para facilitar su procesamiento. Así, en la figura~\ref{fig:ssa} puede apreciarse la transformación de una pequeña función en C en su correspondiente grafo SSA.

\begin{figure}[h]

    \centering\includegraphics[width=0.85\textwidth]{imagenes/ssa.png}

    \caption{Ejemplo de gráfico SSA~\cite{ssa}}

    \label{fig:ssa}
\end{figure}

Como las reglas para construir nodos de un grafo son muy claros se puede construir una gramática BNF que determine las posibilidades. Así, en la figura~\ref{fig:reglas} se muestran las reglas utilizadas por los autores para generar nodos de un grafo SSA junto con los costes asociados, que como puede verse oscilan entre 0 para operaciones de carga de valores constantes hasta 5, para carga de operandos desde memoria.

\begin{figure}[h]

    \centering\includegraphics[width=0.85\textwidth]{imagenes/reglas.png}

    \caption{Reglas gramaticas y costes asociados}

    \label{fig:reglas}
\end{figure}


Así, el problema a resolver consiste entonces en determinar la respuesta a la siguiente pregunta: ¿cuál es la secuencia de transformaciones gramaticales que producen un determinado grafo SSA con el menor coste asociado?

La solución desarrollada consiste en mapear un grafo SSA a otro tipo de problema
llamado PBPQ (Partitioned Boolean Quadratic optimization Problem) que utiliza
vectores que representan los costes de ir de un no-terminal a otros posibles
no-terminales y después obtener el mínimo de todos los costes entre las diversas posibles transiciones de la gramática. Los resultados obtenidos por este método muestran una gran eficiencia en la localización de trozos de código susceptibles de ser convertidos en instrucciones.

\subsection{Codificación de instrucciones}
En~\cite{asip-encoding} puede verse un estudio sobre mecanismos eficientes para la codificación de instrucciones. Esta vertiente del problema puede describirse de la forma siguiente: los microprocesadores normales suelen tener instrucciones basadas en registros donde se aceptan de uno a 3 operandos. Sin embargo, algunas operaciones hechas en los lenguajes de alto nivel pueden involucrar 4 operandos o más por lo que es evidente que la correspondencia entre ambos no es sencilla y que merece la pena estudiar el diseño de juegos de instrucciones.

Partiendo de esta base, los autores utilizan un compilador que utiliza solo un juego de instrucciones trivial. Una vez se compilan los programas se estudian los códigos objeto generados para buscar patrones que ayuden a encontrar instrucciones complejas y determinado después cuales de esos patrones serán lo suficientemente buenos para convertirse en una instrucción.

El principal análisis que se lleva a cabo sobre el código objeto consiste en intentar {\it sintetizar instrucciones}. En la figura adjunta se muestra un ejemplo de como un patrón común consistente en hacer una suma seguida de una operación de carga puede convertirse en una sola instrucción.

\begin{figure}[h]

    \centering\includegraphics[width=0.65\textwidth]{imagenes/instruccionmezclada.png}

    \caption{Fusión de instrucciones}

    \label{fig:fusion}
\end{figure}


La pregunta clave es {\it cada cuantas instrucciones se toma la decisión de unir dichas instrucciones en una sola}. Según sus autores, este número $N$ de instrucciones es parametrizable y además es posible utilizar distintos N dentro de una misma solución. En general, todas las instrucciones. Además, para determinar si las instrucciones generadas son mejores se utiliza una métrica denominada CR o {\it Cycle Reduction} que mide el ahorro de ciclos de reloj que una instrucción fusionada ofrece en comparación con las instrucciones individuales. A medida que se van generando instrucciones se incluyen en una biblioteca de <<posibles instrucciones>> que indican cuantas veces ha aparecido un cierto patrón de instrucción.

{ Los autores de~\cite{asip-encoding} exploran el espacio de posibles instrucciones mediante <<fuerza bruta>>, partiendo de instrucciones resultantes de compilar programas reales}. Aunque ofrece resultados válidos es posible que este proceso pueda mejorarse en base a diversos motivos:
\begin{itemize}
\item{Es muy probable que el análisis de todas las combinaciones posibles dé lugar a muchas combinaciones que no ofrezcan ningún beneficio. Es decir, al analizar los programas compilados y fusionar la instrucción 0 con la 1, la 1 con la 2, la 2 con la 3 y así sucesivamente, se van a analizar conjuntos de forma innecesaria.}
\item{Podría ocurrir que combinaciones de instrucciones que pudieran resultar útiles no hayan aparecido en los programas compilados, por lo que va a resultar imposible que este algoritmo las detecte.}
\item{El orden de complejidad de la heurística utilizada es bastante grande, $O(N*M^{2})$, siendo N el tamaño de la biblioteca y M la longitud media de la secuencia de instrucciones analizada, por lo que solo es factible analizar pequeñas bibliotecas de instrucciones fusionadas.}
\end{itemize}


Una vez obtenida una biblioteca de posibles instrucciones, los autores aplican dos técnicas para intentar elegir cuales son mejores Por un lado se aplican técnicas de la programación lineal para intentar determinar qué conjunto ofrece las mejores reducciones de ciclos con la restricción de que pueda entrar en una cierta anchura de instrucción medida en bits. Por el otro, se utiliza una heurística basada en comprobar si dos o más determinadas instrucción superiores <<cubren>> la labor de una instrucción básica, ya que de ser así, el elegir las dos a la vez no proporciona un ahorro mejor y debería volver a examinarse el coste de ambas y elegir una sola.

\subsection{ISDL}

En~\cite{ISDL} puede encontrarse un trabajo interesante en relación con los conjuntos de instrucciones, aunque orientado en este caso a la creación de compiladores adaptables.

En este trabajo, sus autores exponen un lenguaje para la descripción de juegos de instrucciones que ofrezca una amplia gama de posibilidades:

\begin{itemize}
\item{Dar soporte a diversas arquitecturas}
\item{Permitir restricciones}
\item{Poder crear generadores de código y ensambladores}
\item{Permitir la generación automática de conjuntos de instrucciones}
\end{itemize}


Este lenguaje permite generar descripciones de conjuntos de instrucciones mediante documentos con diversas secciones.

\begin{enumerate}
\item{Formato de instrucción: en esta sección se pueden especificar bloques de bits que tendrán usos reservados, como por ejemplo, usar tres bits para un primer operando y tres para un segundo operando (lo que permitiría usar hasta 8 registros en una hipotética instrucción aritmético-lógica)}
\item{Definiciones globales: probablemente esta la sección más interesante ya que especifica como construir {\bf gramáticas BNF} qe permitan construir las instrucciones en sí. Al generar instrucciones con este mecanismo se puede flexibilizar la especificación de muchos elementos. Por ejemplo, si se dispusiera de 8 registros, una instrucción de transferencia debería especificar 64 combinaciones, mientras que al utilizar una gramática basta con una sola regla que incluya un solo no terminal para todas las posibilidades. Además, ISDL permite que un no terminal tenga código asociado, al igual que otros generadores como Yacc o Bison.}
\item{Recursos de almacenamiento: para especificar las posibilidades de almacenamiento tales como memoria, registros, pila, etc\ldots}
\item{Juego de instrucciones: subdividida en <<nombre de operación>>, <<parámetros>>, <<asignación entre campos de bits>>, <<descripción RTL>>, {\bf <<costes>>} y <<temporización>>}
\item{Restricciones: que pueden ser de tres tipos
\begin{itemize}
\item{Conflictos en la ruta de datos producidos por operaciones que acceden al mismo recurso.}
\item{Conflictos en campos de bits al ocurrir que dos operaciones paralelas modifiquen el mismo bloque de bits.}
\item{Restricciones impuestas por el ensamblador.}
\end{itemize}
}
\item{Detalles arquitectónicos: para especificar información de utilidad para el compilador}
\end{enumerate}

% Local Variables:
%  coding: utf-8
%  mode: flyspell
%  ispell-local-dictionary: "castellano8"
% End:
