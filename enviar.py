#!/usr/bin/python

import sys
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
import os

gmail_user = "profesor.oscar.gomez@gmail.com"
gmail_pwd = "atlantis_2"

def mail(to, subject, text, attach):
   msg = MIMEMultipart()

   msg['From'] = gmail_user
   msg['To'] = to
   msg['Subject'] = subject

   msg.attach(MIMEText(text))

   part = MIMEBase('application', 'octet-stream')
   part.set_payload(open(attach, 'rb').read())
   Encoders.encode_base64(part)
   part.add_header('Content-Disposition',
           'attachment; filename="%s"' % os.path.basename(attach))
   msg.attach(part)

   mailServer = smtplib.SMTP("smtp.gmail.com", 587)
   mailServer.ehlo()
   mailServer.starttls()
   mailServer.ehlo()
   mailServer.login(gmail_user, gmail_pwd)
   mailServer.sendmail(gmail_user, to, msg.as_string())
   # Should be mailServer.quit(), but that crashes...
   mailServer.close()

print sys.argv[1]


mail("profesor.oscar.gomez@gmail.com",
   "Copia de seguridad del trabajo",
   "Copia del trabajo fin de master",
   sys.argv[1])
mail("ogomezgarcia@gmail.com",
   "Copia de seguridad del trabajo",
   "Copia del trabajo fin de master",
   sys.argv[1])
mail("o_gom_gar@hotmail.com",
   "Copia de seguridad del trabajo",
   "Copia del trabajo fin de master",
   sys.argv[1])