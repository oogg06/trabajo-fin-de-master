;;; Directory Local Variables
;;; See Info node `(emacs) Directory Variables' for more information.

((latex-mode
  (coding . utf-8)
  (ispell-dictionary . castellano8)
  (mode . flyspell)
  (fill-column . 80)
  (mode . latex)
  (mode . auto-fill)))
