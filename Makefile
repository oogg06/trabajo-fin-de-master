#LISTA_EXTENSIONES= *.tex *.bib *.sh Makefile *.png cuenta_lineas *.py *.sql *.db 

LISTA_EXTENSIONES=*

all: db tablas doc
	
#Reconstrucción de todo el proyecto
doc:  
	pdflatex tfm.tex; pdflatex tfm.tex; bibtex tfm; pdflatex tfm;pdflatex tfm; evince tfm.pdf; ./estadisticas.sh

tablas:
	./construye_tablas.py
	
db:
	cat articulos.sql | sqlite3 datos.db
clean:
	rm *.pdf *.aux *.log *.bbl *.dvi *~ *.blg *.zip *.toc  
subir:
	git commit -a
	 git push -u origin --all

copia: 
	zip -r Copia.zip $(LISTA_EXTENSIONES) plantillas 
	#thunderbird -compose "to=profesor.oscar.gomez@gmail.com,bcc='ogomezgarcia@gmail.com,o_gom_gar@hotmail.com',subject='Copia TFM `date`',body='Copia de seguridad del trabajo fin de master',attachment='`pwd`/Copia.zip'"
	./enviar.py Copia.zip &
