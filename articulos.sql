drop table id_articulos;
drop table tags;
drop table articulo_tiene_tag;

create table id_articulos(id_articulo varchar(30) primary key, tam integer);
create table tags(tag varchar(30) primary key);
create table articulo_tiene_tag(id_articulo varchar(30), tag varchar(30), primary key(id_articulo, tag), foreign key (id_articulo) references id_articulos(id_articulo), foreign key (tag) references tags(tag) );


--Sistema operativo
insert into tags values('so');     
--VM Java
insert into tags values('vmjava');
--VM No java
insert into tags values('vmnojava');

insert into tags values('pila');
insert into tags values('reg');

--Para MC
insert into tags values('mc');
--Para wsn
insert into tags values('wsn');

--Plantilla
--insert into id_articulos values('');
--insert into articulo_tiene_tag values ('', 'so');
--insert into articulo_tiene_tag values ('', 'vmnojava');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('dvm', 334);
insert into articulo_tiene_tag values ('dvm', 'vmnojava');
insert into articulo_tiene_tag values ('dvm', 'pila');

insert into id_articulos values('nanovm', 8192);
insert into articulo_tiene_tag values ('nanovm', 'vmjava');
insert into articulo_tiene_tag values ('nanovm', 'mc');
insert into articulo_tiene_tag values ('nanovm', 'pila');


insert into id_articulos values('sos', 0);
insert into articulo_tiene_tag values ('sos', 'so');
insert into articulo_tiene_tag values ('sos', 'wsn');

insert into id_articulos values('lorien',28672);
insert into articulo_tiene_tag values ('lorien', 'so');
insert into articulo_tiene_tag values ('lorien', 'wsn');

insert into id_articulos values('contiki', 0);
insert into articulo_tiene_tag values ('contiki', 'so');
insert into articulo_tiene_tag values ('contiki', 'wsn');
insert into articulo_tiene_tag values ('contiki', 'mc');

insert into id_articulos values('sensorware', 184320);
insert into articulo_tiene_tag values ('sensorware', 'vmnojava');
insert into articulo_tiene_tag values ('sensorware', 'wsn');

insert into id_articulos values('tinyreef',0);
insert into articulo_tiene_tag values ('tinyreef', 'vmnojava');
insert into articulo_tiene_tag values ('tinyreef', 'wsn');
insert into articulo_tiene_tag values ('tinyreef', 'reg');


insert into id_articulos values('esterelvm',814);
insert into articulo_tiene_tag values ('esterelvm', 'vmnojava');
insert into articulo_tiene_tag values ('esterelvm', 'mc');
insert into articulo_tiene_tag values ('esterelvm', 'reg');

insert into id_articulos values('esterel', 0);
insert into id_articulos values('magnetos', 0);
insert into articulo_tiene_tag values ('magnetos', 'vmjava');
insert into articulo_tiene_tag values ('magnetos', 'wsn');
insert into articulo_tiene_tag values ('magnetos', 'pila');

insert into id_articulos values('picojava', 0);
insert into articulo_tiene_tag values ('picojava', 'vmjava');
insert into articulo_tiene_tag values ('picojava', 'pila');

insert into id_articulos values('javavm', 0);

insert into id_articulos values('squawk', 0);
insert into articulo_tiene_tag values ('squawk', 'vmjava');
insert into articulo_tiene_tag values ('squawk', 'wsn');
insert into articulo_tiene_tag values ('squawk', 'pila');

insert into id_articulos values('tinyos', 0);
insert into articulo_tiene_tag values ('tinyos', 'so');
insert into articulo_tiene_tag values ('tinyos', 'wsn');
insert into articulo_tiene_tag values ('tinyos', 'mc');

insert into id_articulos values('vms-wsn', 0);

insert into id_articulos values('mantisos', 500);
insert into articulo_tiene_tag values ('mantisos', 'so');
insert into articulo_tiene_tag values ('mantisos', 'wsn');
--insert into articulo_tiene_tag values ('', 'vmnojava');
--insert into articulo_tiene_tag values ('', 'vmjava');


insert into id_articulos values('liteos', 0);
insert into articulo_tiene_tag values ('liteos', 'so');
insert into articulo_tiene_tag values ('liteos', 'wsn');
--insert into articulo_tiene_tag values ('', 'vmnojava');
--insert into articulo_tiene_tag values ('', 'vmjava');


insert into id_articulos values('tkernel', 28864);
insert into articulo_tiene_tag values ('tkernel', 'so');
insert into articulo_tiene_tag values ('tkernel', 'wsn');
--insert into articulo_tiene_tag values ('', 'vmnojava');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('nanork', 0);
insert into articulo_tiene_tag values ('nanork', 'so');
insert into articulo_tiene_tag values ('nanork', 'mc');

insert into id_articulos values('avrx', 0);
insert into articulo_tiene_tag values ('avrx', 'so');
insert into articulo_tiene_tag values ('avrx', 'mc');
--insert into articulo_tiene_tag values ('', 'vmnojava');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('emeralds', 13312);
insert into articulo_tiene_tag values ('emeralds', 'so');
insert into articulo_tiene_tag values ('emeralds', 'mc');


insert into id_articulos values('valentine', 0);
insert into articulo_tiene_tag values ('valentine', 'so');
insert into articulo_tiene_tag values ('valentine', 'wsn');

insert into id_articulos values('osek', 0);
insert into articulo_tiene_tag values ('osek', 'so');
insert into articulo_tiene_tag values ('osek', 'mc');

insert into id_articulos values('ucos', 24576);
insert into articulo_tiene_tag values ('ucos', 'so');
insert into articulo_tiene_tag values ('ucos', 'mc');


insert into id_articulos values('dalvik', 0);
--insert into articulo_tiene_tag values ('', 'so');
--insert into articulo_tiene_tag values ('', 'vmnojava');
insert into articulo_tiene_tag values ('dalvik', 'vmjava');
insert into articulo_tiene_tag values ('dalvik', 'pila');


insert into id_articulos values('jala', 0);
--insert into articulo_tiene_tag values ('', 'so');
--insert into articulo_tiene_tag values ('', 'vmnojava');
insert into articulo_tiene_tag values ('jala', 'vmjava');
insert into articulo_tiene_tag values ('jala', 'pila');

insert into id_articulos values('mate', 16892);
insert into articulo_tiene_tag values ('mate', 'vmnojava');
insert into articulo_tiene_tag values ('mate', 'wsn');
insert into articulo_tiene_tag values ('mate', 'pila');

--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('scy', 0);
insert into articulo_tiene_tag values ('scy', 'vmnojava');
insert into articulo_tiene_tag values ('scy', 'wsn');
insert into articulo_tiene_tag values ('scy', 'reg');


insert into id_articulos values('moterunner',25600);
insert into articulo_tiene_tag values ('moterunner', 'vmnojava');
insert into articulo_tiene_tag values ('moterunner', 'mc');
insert into articulo_tiene_tag values ('moterunner', 'wsn');
insert into articulo_tiene_tag values ('moterunner', 'pila');

insert into id_articulos values('swissqm', 4096);
--insert into articulo_tiene_tag values ('', 'so');
insert into articulo_tiene_tag values ('swissqm', 'vmjava');
insert into articulo_tiene_tag values ('swissqm', 'wsn');
insert into articulo_tiene_tag values ('swissqm', 'pila');

insert into id_articulos values('nutos', 0);
insert into articulo_tiene_tag values ('nutos', 'so');
insert into articulo_tiene_tag values ('nutos', 'mc');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('vmstar', 10500);
insert into articulo_tiene_tag values ('vmstar', 'vmjava');
insert into articulo_tiene_tag values ('vmstar', 'wsn');
insert into articulo_tiene_tag values ('vmstar', 'pila');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('davim', 4096);
insert into articulo_tiene_tag values ('davim', 'vmnojava');
insert into articulo_tiene_tag values ('davim', 'wsn');


insert into id_articulos values('asvm', 0);
insert into articulo_tiene_tag values ('asvm', 'vmnojava');
insert into articulo_tiene_tag values ('asvm', 'wsn');
insert into articulo_tiene_tag values ('asvm', 'mc');
insert into articulo_tiene_tag values ('asvm', 'pila');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('darjeeling', 8192);
insert into articulo_tiene_tag values ('darjeeling', 'vmjava');
insert into articulo_tiene_tag values ('darjeeling', 'wsn');
insert into articulo_tiene_tag values ('darjeeling', 'pila');
--insert into articulo_tiene_tag values ('', 'vmjava');

insert into id_articulos values('vmscript', 0);
insert into articulo_tiene_tag values ('vmscript', 'vmnojava');
insert into articulo_tiene_tag values ('vmscript', 'wsn');
insert into articulo_tiene_tag values ('vmscript', 'pila');

insert into id_articulos values('agilla', 58368);
insert into articulo_tiene_tag values ('agilla', 'vmnojava');
insert into articulo_tiene_tag values ('agilla', 'wsn');
insert into articulo_tiene_tag values ('agilla', 'pila');

insert into id_articulos values('vvm', 0);
insert into articulo_tiene_tag values ('vvm', 'vmnojava');

.quit
