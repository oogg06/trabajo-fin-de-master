#!/usr/bin/python
# coding=utf-8


#Prerequisitos
# 1. Se debe tener la lista de señales de la ruta de datos (R1IN, R1OUT, READ WRITE, MARIN, etc...)
# 2. El juego de instrucciones debe usar al menos UNA vez todas ellas
# 3. Cada instrucción puede tener de uno a tres ciclos de instrucción. En cada ciclo solo puede haber un OUT, aunque varios IN

#Algoritmo para juegos basados en registros
#Se elige una de estas instrucciones
#   1. Carga, que puede tener dos operandos
#   2. Aritmético lógica, que puede tener dos, tres, cuatro operandos
#   3. Salto 
#       3.1 Salto (condicion) (donde)
#               (condicion pueden ser varias cosas): Z, NE, GT...
#               (donde) pueden ser varias cosas +128, -127 para saltos relativos, direcciones absolutas de 16 bits,
#                           contenido de registros, contenido de registros indirectos, 

import sys
import re
import string

class Instruccion(object):
    
    #Algunas instrucciones manejan valores absolutos o direcciones absolutas
    
    def __init__(self, patron, tamanio, codop):
        self.patron             =   patron
        self.tam_en_bytes       =   tamanio
        self.codigo_operacion   =   codop
        self.valores             =   []
        self.bytecodes           =   []
    
    
    def get_bytes(self, lista):
        resultado=self.bytecodes[:]
        #print resultado
        for valor in lista:
            resultado.append(valor)
        return resultado

    
    def reemplazar_valores(self, linea, etiquetas):
        #print etiquetas
        #print "Linea %s " % linea
        devolver=[]
        
        for (clave, valor) in etiquetas.iteritems():
            #print "Clave %s, valor %s " %(clave, valor)
            if re.search(clave, linea):
            #if string.find(linea, " "+clave+" ")!=-1:
                devolver.append(valor)
                #print "Reemplazando en %s" % linea
                #linea.replace(clave, str(valor))
                #print "Linea: %s" % linea
        #print self.patron
        return devolver
        
etiquetas=dict()
        
ESPACIOS    ="[ \t]+"
NUMERO      ="([0-9]+)"
ORG         ="(ORG)"
DW          ="(DW)"
ETIQUETA    ="(:[A-Z]+)"
PALABRA     ="([A-Z]+)"
VALOR       ="\(([A-Z]+)\)"
COMA        =","
SEPARACION  =ESPACIOS + COMA + ESPACIOS

INSTRUCCION_ORG =   ESPACIOS + ORG + ESPACIOS + NUMERO
i0=Instruccion(INSTRUCCION_ORG, 0, "")

INSTRUCCION_DW  =   ESPACIOS+"?" + DW +ESPACIOS + PALABRA  +ESPACIOS+ NUMERO
i1=Instruccion(INSTRUCCION_DW, 0, "")

INSTRUCCION_ADD = ETIQUETA+"?" +  ESPACIOS + "ADD" + ESPACIOS + PALABRA + ESPACIOS + "R1"



i2=Instruccion(INSTRUCCION_ADD, 2, "")
i2.bytecodes.append(65)

I_ADD = ETIQUETA+"?" +  ESPACIOS + "ADD" + ESPACIOS + VALOR + ESPACIOS + VALOR
i3=Instruccion(I_ADD, 3, "")
print i2.bytecodes
i3.bytecodes.append(42)
print i3.bytecodes
instrucciones=[]
instrucciones.append(i0)
instrucciones.append(i1)
instrucciones.append(i2)
instrucciones.append(i3)


def obtener_etiquetas(lineas):
    """Dadas las lineas del archivo ensamblador, obtiene las etiquetas y valores
    definidos como constantes por instrucciones del tipo DW. Devuelve un
    diccionario con las etiquetas y el offset total"""
    offset=0
    etiquetas=dict()
    for linea in lineas:
        contiene_etiqueta=re.search(ETIQUETA, linea)
        if contiene_etiqueta:
            etiqueta_encontrada=contiene_etiqueta.group(0)
            #print "Hallada etiqueta:%s" % etiqueta_encontrada[1:]
            
            etiquetas[etiqueta_encontrada[1:]]=offset
        contiene_definicion=re.search(INSTRUCCION_DW, linea)
        if contiene_definicion:
            variable=contiene_definicion.group(2)
            valor=contiene_definicion.group(3)
            #print variable
            etiquetas[variable]=valor
        
        for instruccion in instrucciones:
            resultado=re.search(instruccion.patron, linea)
            if resultado!=None:
                #print "Instruccion localizada"
                #print "Offset %i " % offset
                offset=offset+instruccion.tam_en_bytes
    return (etiquetas, offset)
    
if len(sys.argv)!=2:
    print "Proporcione el nombre del archivo a ensamblar."
    sys.exit(-1)
archivo=sys.argv[1]

print "Procesando archivo: %s" % archivo

with open (archivo) as f:
    lineas=f.readlines()

offset=0
(etiquetas, offset) = obtener_etiquetas(lineas)
for linea in lineas:
    #print "Procesando %s" % linea
    for instruccion in instrucciones:
        resultado=re.search(instruccion.patron, linea)
        if resultado!=None:
            #print "Hallado mnemonico %s" % instruccion.patron
            valores=instruccion.reemplazar_valores(linea, etiquetas)
            lista_bytes=instruccion.get_bytes(valores)
            print lista_bytes


