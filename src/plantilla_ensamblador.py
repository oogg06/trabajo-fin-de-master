#!/usr/bin/python
# coding=utf-8
import sys
import re

from ply import lex

cadena_ADD="ADD"
cadena_SUB="SUB"
cadena_XOR="XOR"
cadena_AND="AND"
cadena_JMP="JMP"
cadena_JNZ="JNZ"
cadena_JGE="JGE"
cadena_JZ="JZ"
cadena_JE="JE"




tokens=(cadena_ADD, cadena_SUB, cadena_XOR, cadena_AND,
        cadena_JMP, cadena_JZ, cadena_JNZ, cadena_JE, cadena_JGE,
        "CORCHETE_ABIERTO", "CORCHETE_CERRADO", "ETIQUETA")

#Se generan dinámicamente los tokens registro y las expresiones regulares asociadas
for x in range(128):
    nombre="R"+str(x)
    globals()[nombre]=re.compile(nombre)
    
t_ADD   = r"ADD"
t_SUB   = r"SUB"
t_XOR   = r"XOR"
t_AND   = r"AND"
t_JMP   = r"JMP"
t_JNZ   = r"JNZ"
t_JE    = r"JE"
t_JGE   = r"JGE"
t_CORCHETE_ABIERTO="r\["
t_CORCHETE_CERRADO="r\]"


def t_ETIQUETA(t):
    r':[A-Z]+'
    return t
def t_VARIABLE(t):
    r'[A-Z]+'
    return t
def t_NUMERO(t):
    r'[0-9]+'
    return t
def t_error(t):
    print("Símbolo ilegal %s'" % t.value[0])
    t.lexer.skip(1)

t_ignore = " \t"
lexer=lex.lex()

def p_programa(p):
    'programa : listainstrucciones'
    print "Ejecutable generado correctamente"
    
def p_lista_instrucciones(p):
    """listainstrucciones : instruccion
                        | instruccion listainstrucciones"""
    pass



#Espacio reservado para ser rellenado por el generador de instrucciones
#No toques el código aquí incluido

#Fin de instrucciones auto-generadas


def ensamblar_archivo():
    #Estas líneas leen el contenido del archivo
    if len(sys.argv)!=2:
        print "Uso: %s <nombre_de_archivo>" % sys.argv[0]
        sys.exit(-1)
    
    archivo=open(sys.argv[1], "r")
    lineas=archivo.read()
    archivo.close()
    
    #Y a partir de aquí se procesa el archivo
    from ply import yacc
    parser = yacc.yacc()
    parser.parse(lineas)
    
if __name__ == '__main__':
    ensamblar_archivo()