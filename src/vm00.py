#!/usr/bin/env python
# coding=utf-8
import random
import io
import plantilla_ensamblador

class Registro(object):
    nombre=""
    contenido=0
    ancho=0
    def __init__(self, nombre, ancho_bits):
        self.nombre=nombre
        self.ancho=ancho_bits
    def limpiar(self):
        self.contenido=0
    def set_valor(self, valor):
        self.contenido=valor
    def get_valor(self):
        return self.contenido
    def __repr__(self):
        return self.nombre
    def incrementar(self):
        self.contenido=self.contenido+1
        if (self.contenido>2**self.ancho):
            self.contenido=0
            
class Memoria(object):
    def __init__(self, tamanio, ancho_de_palabra):
        self.tamanio=tamanio
        self.contenido=[0 for i in range(tamanio)]
        self.ancho_de_palabra=ancho_de_palabra
    def llenar_de_basura(self):
        max=2**self.ancho_de_palabra
        max=max-1
        self.contenido=[random.randint(0, max) for i in range(self.tamanio)]
    def leer_posicion(self, pos):
        return self.contenido[pos]
    def escribir_posicion(self, pos, valor):
        self.contenido[pos]=valor
    def get_tam(self):
        return self.tamanio
    def cargar_bytecodes(self, lista):
        for pos in range(len(lista)):
            self.contenido[pos]=lista[pos]
            
            

class CPU(object):
    
    registros=dict()
    memoria=None
    hay_un_resultado_cero=False
    def __init__(self, tam_memoria_en_posiciones, ancho_palabra, num_registros=0):
        self.memoria=Memoria(tam_memoria_en_posiciones, ancho_palabra)
        self.memoria.llenar_de_basura()
        registro_pc=Registro("PC", ancho_palabra)
        registro_pc.set_valor(0)
        self.registros["PC"]=registro_pc
        for i in range(num_registros):
            nombre="R"+str(i)
            r=Registro(nombre, ancho_palabra)
            self.registros[nombre]=r

    def cargar_valor_en_registro(self, nombre_registro, valor):
        self.registros[nombre_registro].set_valor(valor)
        
    def get_valor_registro(self, nombre_registro):
        return self.registros[nombre_registro].get_valor()
        
    def get_contenido_posicion_memoria(self, posicion):
        pos=0
        if isinstance(posicion, Registro):
            pos=posicion.get_valor()
        if isinstance(posicion, int):
            pos=posicion
        return self.memoria.leer_posicion(pos)
        
    def set_contenido_posicion_memoria(self, posicion, valor):
        pos=0
        if isinstance(posicion, Registro):
            pos=posicion.get_valor()
        if isinstance(posicion, int):
            pos=posicion
        return self.memoria.escribir_posicion(pos, valor)
        
    def leer_pc(self):
        """Lee el siguiente valor de memoria al que apunta el PC
        e incrementa dicho registro"""
        posicion=self.registros["PC"].get_valor()
        self.registros["PC"].incrementar()
        return self.memoria.leer_posicion(posicion)
    
    def leer_word(self):
        """Lee un valor de 16 bits a partir de donde apunte el PC"""
        valor_pc=self.registros["PC"].get_valor()
        byte_mas_significativo=self.memoria.leer_posicion(valor_pc)
        self.registros["PC"].incrementar()
        valor_pc=self.registros["PC"].get_valor()
        byte_menos_significativo=self.memoria.leer_posicion(valor_pc)
        self.registros["PC"].incrementar()
        
        print "BMS:"+str(byte_mas_significativo)
        print "bms:"+str(byte_menos_significativo)
        return ord(byte_menos_significativo) + (256*(ord( byte_mas_significativo)))
        
    def sumar(self, valor1, valor2):
        resultado=valor1+valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def restar(self, valor1, valor2):
        resultado=valor1-valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def multiplicar(self, valor1, valor2):
        resultado=valor1*valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def dividir(self, valor1, valor2):
        resultado=valor1/valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def hacer_and(self, valor1, valor2):
        resultado=valor1&valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def hacer_or(self, valor1, valor2):
        resultado=valor1|valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def hacer_xor(self, valor1, valor2):
        resultado=valor1^valor2
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def hacer_not(self, valor1):
        resultado=~valor1
        if resultado==0:
            self.hay_un_resultado_cero=True
        else:
            self.hay_un_resultado_cero=False
        return resultado
    def ejecutar_archivo_bytecodes(self, nombre_archivo):
        secuencia_bytes=open(nombre_archivo, "rb").read()
        print secuencia_bytes
        self.memoria.cargar_bytecodes(secuencia_bytes)
        
        if self.leer_pc==0:
            self.cargar_valor_en_registro('R7', reduce(self.dividir,[self.get_valor_registro('R7'),self.get_contenido_posicion_memoria('R4'),self.leer_word()]))
            
        if self.leer_pc==1:
            self.set_contenido_posicion_memoria('R4', reduce(self.multiplicar,[self.get_contenido_posicion_memoria('R4'),self.get_contenido_posicion_memoria('R1'),self.get_contenido_posicion_memoria('R4')]))
            
        if self.leer_pc==2:
            self.set_contenido_posicion_memoria('R1', reduce(self.hacer_xor,[self.get_contenido_posicion_memoria('R1'),self.get_valor_registro('R4'),self.get_contenido_posicion_memoria('R5')]))
            
        if self.leer_pc==3:
            self.set_contenido_posicion_memoria('R4', reduce(self.multiplicar,[self.get_contenido_posicion_memoria('R4'),self.leer_word(),self.leer_word()]))
            
        if self.leer_pc==4:
            self.cargar_valor_en_registro('R4', reduce(self.hacer_or,[self.get_valor_registro('R4'),self.get_contenido_posicion_memoria('R3'),self.get_valor_registro('R7')]))
            
        

archivo=open("archivo.exe", "wb")
archivo.write("\x40\x40\x7E")
archivo.close()
cpu=CPU(32, 16, 8)
print cpu.memoria.contenido
r=Registro("R0", 0)
cpu.ejecutar_archivo_bytecodes("archivo.exe")
print cpu.memoria.contenido
x=cpu.leer_word()
print "Word:"+str(x)