#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import random

def num_linea():
    return inspect.currentframe().f_back.f_lineno
def entero_al_azar(min, max):
    return random.randint(min, max)
def valor_al_azar():
    return random.random()


class ExcepcionNoHayPosiblesValores(Exception):
    nombre=""
    def __init__(self, nombre):
        self.nombre_gen=nombre
    def __str__(self):
        return "No hay una lista de posibles valores para "+self.nombre

class ExcepcionProbabilidadInvalida(Exception):
    valor=0
    def __init__(self, valor):
        self.valor=valor
    def __str__(self):
        return "Probabilidad invalida: " + str ( self.valor )

class ExcepcionPosGenInvalida(object):
    pos=0
    def __init__(self, pos):
        self.pos=pos
    def __str__(self):
        return "Posicion invalida de gen:" + str ( self.pos )


class Gen(object):
    nombre=""
    posibles_valores=[]
    valor=""
    funcion_ajuste=None
    def __str__(self):
        return self.nombre
    
    def set_posibles_valores(self, posibles_valores):
        self.posibles_valores=posibles_valores
    
    def set_valor_al_azar(self):
        if self.posibles_valores==[]:
            raise ExcepcionNoHayPosiblesValores(self.nombre)
        
        primer_valor    =   0
        ultimo_valor    =   len ( self.posibles_valores ) - 1
        pos_al_azar     =   entero_al_azar ( primer_valor, ultimo_valor )
        self.valor      =   self.posibles_valores [ pos_al_azar ]
    
    def set_valor(self, arg1):
        self.valor=arg1
    
    def __str__(self):
        return self.valor
    
    def __eq__(self, gen):
        """Nos dice si dos genes son iguales"""
        if self.valor==gen.valor:
            return True
        return False
    
class CadenaGenes(object):
    lista_genes=[]
    probabilidad=-1
    num_generacion=0
    parametros_funcion_ajuste=None
    
    def __init__(self, longitud):
        self.lista_genes=[ Gen() for pos in range(longitud)]
        
    
    def crear_cadena_al_azar(self, longitud, posibles_valores):
        self.lista_genes=[ Gen() for pos in range(longitud)]
        for pos in range(longitud):
            self.lista_genes [ pos ].set_posibles_valores(posibles_valores)
            self.lista_genes [ pos ].set_valor_al_azar()
    
    def set_posibles_valores(self, posibles_valores ):
        for pos in range ( len (self.lista_genes ) ) :
            self.lista_genes [ pos ].set_posibles_valores ( posibles_valores )
    
    def set_valor_gen(self, pos_gen, valor):
        if pos_gen<0 or pos_gen>len(self.lista_genes):
            raise ExcepcionPosGenInvalida(pos_gen)
        self.lista_genes[pos_gen].set_valor(valor)
    
    def set_valor_al_azar(self):
        for pos in range ( len (self.lista_genes ) ) :
            self.lista_genes [ pos ].set_valor_al_azar()
    
    def __str__(self):
        return "".join( map (str, self.lista_genes ) )
    
    def set_probabilidad_mutacion(self, probabilidad):
        if probabilidad < 0 or probabilidad > 1:
            raise ExcepcionProbabilidadInvalida ( probabilidad ) 
        self.probabilidad=probabilidad
        
    def evolucionar(self):
        for pos in range ( len (self.lista_genes ) ) :
            valor=valor_al_azar()
            if valor < self.probabilidad:
                self.lista_genes [ pos ].set_valor_al_azar()
        self.num_generacion+=1
    
    def set_funcion_ajuste(self, fn, parametros):
        self.funcion_ajuste=fn
        self.parametros_funcion_ajuste=parametros
    
    def valor_ajuste(self):
        return self.funcion_ajuste(self, self.parametros_funcion_ajuste)
    
    def mas_ajustado_que(self, otro_individuo):
        """Nos dice si nuestro individuo tiene un valor de ajuste mejor que otro"""
        return self.funcion_ajuste(
                self, parametros_funcion_ajuste) > \
                    self.funcion_ajuste(otro_individuo, parametros_funcion_ajuste)

    def __iter__(self):
        return iter(self.lista_genes)
    def __getitem__(self, pos):
        return self.lista_genes[pos]
    def __len__(self):
        return len ( self.lista_genes )
    
    
class Individuo(object):
    """Un individuo tiene una secuencia de cadenas de genes"""
    cadenas_genes=[]


class Poblacion(object):
    individuos=[]
    funcion_ajuste=None
    parametros_funcion_ajuste=None
    #Valores de ajuste de cada uno de los individuos
    valores_ajuste=[]
    
    
    def anadir_individuo(self, individuo):
        self.individuos.append(individuo)
    def set_funcion_ajuste(self, fn, parametros):
        for i in self.individuos:
            print i
            i.set_funcion_ajuste(fn, parametros)
        self.funcion_ajuste=fn
        self.parametros_funcion_ajuste=parametros
    
    def calcular_valores_ajuste(self):
        self.valores_ajuste=[i.valor_ajuste() for i in self.individuos]
    
    def marcar_los_adaptados(self, arg1):
        """Se comprueba cuales son los mejor adaptados, que serán los que pasen a la siguiente generacion"""
        pass
    
    

def funcion_ajuste1(cadena_genes, cadena):
    if len(cadena_genes)!=len(cadena):
        return 0
    valor=0
    for pos in range( len(cadena_genes) ):
        if cadena[pos]==cadena_genes[pos]:
            valor+=1
    return valor
    


    
   
a=Gen()
a.set_valor("d")
print a
a.set_posibles_valores ( ["a", "b", "c", "d" ] )
a.set_valor_al_azar()
print a

adn=CadenaGenes(20)
adn.set_posibles_valores ( ["a", "b", "c", "d" ] )
adn.set_valor_al_azar()
adn.set_probabilidad_mutacion(0.1)
print adn
adn.evolucionar()
print adn
print

def x (a,b):
    print "Parametro 1 %s parametro 2 %s" % (a, b)

def y(z, t):
    z(*t)

#y (x, ( "aaa", "bbb" ))


adn3=CadenaGenes(20)
adn3.crear_cadena_al_azar(20, ["a", "b", "c", "d"])


adn4=CadenaGenes(20)
adn4.crear_cadena_al_azar(20, ["a", "b", "c", "d"])


adn5=CadenaGenes(20)
adn5.crear_cadena_al_azar(20, ["a", "b", "c", "d"])


poblacion=Poblacion()

poblacion.anadir_individuo(adn4)
poblacion.anadir_individuo(adn5)
poblacion.anadir_individuo(adn3)
poblacion.set_funcion_ajuste (funcion_ajuste1, adn)
poblacion.calcular_valores_ajuste()
print poblacion.valores_ajuste