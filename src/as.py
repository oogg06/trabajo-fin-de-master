#!/usr/bin/env python
# coding=utf-8

from ply import lex, yacc

tokens=("ORG", "DW", "NUMERO")
    
t_ORG="ORG"
t_DW="DW"

def t_NUMERO(t):
    r'\d+'
    try:
        t.value=int(t.value)
    except ValueError:
        print "ERROR: entero demasiado grande"
        t.value=0
    return t
def t_error(t):
    print ("Error en %s" % t.value[0])
    
t_ignore=" \t\n"

datos="""
    ORG 20000
    DW 200
"""

#lexer=lex.lex()
#lexer.input(datos)
#while True:
#    tok=lexer.token()
#    if not tok:
#        break
#    print tok



def p_instruccion1(regla):
    'instruccion1 : ORG NUMERO'
    print "Encontrada instruccion 1"
def p_error(p):
    print "Error de análisis!"
    
parser=yacc.yacc()
while True:
    result=parser.parse(datos)
    print result