#!/usr/bin/env python

import vm
import pyglet

ventana=pyglet.window.Window()
label = pyglet.text.Label('Hello, world',
                          font_name='Times New Roman',
                          font_size=36,
                          x=ventana.width//2, y=ventana.height//2,
                          anchor_x='center', anchor_y='center')

@ventana.event
def on_draw():
    ventana.clear()
    label.draw()

pyglet.app.run()