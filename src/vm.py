#!/usr/bin/env python
# coding=utf-8
import unittest
from time import sleep

#Medida en Kb
MEMORIA=64

#Ancho de palabra del procesador virtual
#Medido en bits
ANCHO_PALABRA=16

#Numero de registros de la máquina virtual
REGISTROS=6

#Tamaño en bits de los registros
ANCHO_REGISTROS=8


#OPERACIONES DE LA ALU
ALU_SUMA    =   0
ALU_RESTA   =   1
ALU_MULTI   =   2
ALU_DIV     =   3

class CadenaFija:
    #Estas cadenas almacenarán su longitud al estilo Pascal
    longitud=0
    bytes=[]
    def __init__(self):
        pass
    def __init__(self, longitud, cadena):
        self.longitud=longitud
        bytes=cadena
    

class Entero:
    bits=0
    valor=0
    def __init__(self, valor):
        self.valor=valor
    
    
#Un suscriptor es cualquier elemento de la ruta de datos: el PC, un registro,
#la ALU (que tendrá más de un valor)
class Suscriptor(object):
    #Un suscriptor puede tener muchas entradas
    num_entradas=1
    num_salidas=1
    #Valores en las entradas
    valores_entrada=[]
    #Indican si la entrada está activada
    entrada_activada=[]
    
    #Valores en las salidas
    valores_salida=[]
    #Indican si las salidas están activadas
    salida_activada=[]
    
    suscriptores=[]
    
    nombre=""
    def __init__(self, num_entradas=1, num_salidas=1):
        
        #Extendemos la lista
        self.valores_entrada    =   range(0, num_entradas)
        self.valores_salida     =   range(0, num_salidas)
        self.entrada_activada   =   range(0, num_entradas)
        self.salida_activada    =   range(0, num_salidas)
        
        for i in range(0, num_entradas):
            self.valores_entrada[i]     =   0
            self.entrada_activada[i]    =   False
            
        for j in range(0, num_salidas):
            self.valores_salida[j]      =   0
            self.salida_activada[j]     =   False
    def __str__(self):
        descripcion_valores_entrada     =   [str(valor) for valor in self.valores_entrada]
        descripcion_estado_entradas     =   [str(estado) for estado in self.entrada_activada]
        
        descripcion_entradas1="[" + "-".join(descripcion_estado_entradas) +  "]"
        descripcion_entradas2="[" + "-".join(descripcion_valores_entrada) +  "]"
        
        return descripcion_entradas1+"\n"+descripcion_entradas2
    
    def set_nombre(self, nombre):
        self.nombre=nombre
    def get_nombre(self):
        return self.nombre
    
    def get_valor_entrada(self, num_pos=0):
        return self.valores_entrada[num_pos]
        
    def entrada_esta_activada(self, num_pos=0):
        return self.entrada_activada[num_pos]
        
    def salida_esta_activada(self, num_pos=0):
        return self.salida_activada[num_pos]
        
    def activar_entrada(self, num_pos=0):
        self.entrada_activada[num_pos]=True
    
    def desactivar_entrada(self, num_pos=0):
        self.entrada_activada[num_pos]=False
        
    def activar_salida(self, num_pos=0):
        self.salida_activada[num_pos]=True
    
    def desactivar_salida(self, num_pos=0):
        self.salida_activada[num_pos]=False
        
    def set_valor_entrada(self, valor, num_pos=0):
        if self.entrada_activada[num_pos]==False:
            return
        self.valores_entrada[num_pos]=valor
        
    def get_valor_salida(self, num_pos=0):
        return self.valores_salida[num_pos]
        
    """Establece un valor de salida en una cierta posición"""
    def set_valor_salida(self, valor, num_pos=0):
        if self.salida_activada[num_pos]==False:
            return
        self.valores_salida[num_pos]=valor
        
    def procesar():
        pass
    
    """Un cierto suscriptor conecta su entrada a la salida de este elemento"""
    def anadirSuscriptor(self, salida, suscriptor, entrada):
        tupla=(salida, suscriptor, entrada)
        self.suscriptores.append ( tupla )
            
    def notificar(self):
        for tupla in self.suscriptores:
            (num_salida, suscriptor, num_entrada)=tupla
            valor_salida=self.get_valor_salida( num_salida )
            suscriptor.set_valor_entrada(valor_salida, num_entrada)


class Registro(Suscriptor):
    #Por comodidad,cuando un registro recibe un valor bloquea nuevas entradas
    def set_valor_entrada(self, valor, num_pos=0):
        self.valores_entrada[num_pos]=valor
        self.desactivar_entrada(num_pos)
        
    #Un registro simplemente pone en su salida 0 lo que hay en su entrada 0
    def procesar(self):
        entrada=self.get_valor_entrada()
        self.activar_salida()
        self.set_valor_salida(entrada)
        self.notificar()
        
class Bus(Suscriptor):
    bits=0
    valor=0
    
class ALU_OP_NO_VALIDA(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Se seleccionó una operación no válida para la ALU"


class Memoria(Suscriptor):
    retardo=0
    datos=[]
    registro_mar=Registro()
    registro_mdr=Registro()
    def __init__(self, retardo, tamanio):
        Suscriptor.__init__(self, 1, 1)
        self.datos=range(0, tamanio)
        self.retardo=retardo
    def set_registro_mar(self, dato):
        pass
    def set_registro_mdr(self, dato):
        pass
    def set_mar(self, direccion):
        self.registro_mar.activar_entrada()
        self.registro_mar.set_valor_entrada(direccion)
        
    def get_mar(self):
        return self.registro_mar.get_valor_entrada()
    def get_mdr(self):
        return self.registro_mdr.get_valor_entrada()
    
    def procesar(self):
        print "Recuperando dato de memoria..."
        sleep(1)
        self.activar_salida()
        direccion=self.get_valor_entrada()
        salida=datos[direccion]
        self.set_salida( salida )
        
        
class ALU(Suscriptor):
    operacion=0
    def seleccionar_operacion(self, operacion):
        self.operacion=operacion
    def suma(self, a, b):
        return a+b
    def resta(self, a, b):
        return a-b
    def multi(self, a, b):
        return a*b
    def division(self, a, b):
        return a/b
    
    def get_funcion_a_aplicar(self):
        if self.operacion==ALU_SUMA:
            return self.suma
        elif self.operacion==ALU_RESTA:
            return self.resta
        elif self.operacion==ALU_MULTI:
            return self.multi
        elif self.operacion==ALU_DIV:
            return self.division
        #Si no hay una operación valida se produce una excepcion
        raise ALU_OP_NO_VALIDA()
        
    def procesar(self):
        funcion=self.get_funcion_a_aplicar()
        resultado=reduce(funcion, self.valores_entrada)
        
        self.set_valor_salida(resultado)
        

"""Una microinstrucción es una operación muy simple sobre la ruta de datos, normalmente
volcar los contenidos de un registro al bus, meter en el registro los contenidos del bus
o efectuar una operación aritmético-lógica"""
class Microinstruccion:
    posicion=0
    registro=None
    descripcion=""
    
    def descripcion_operacion(self):
        return "("  +   self.descripcion +")"
    def descripcion_posicion(self):
        if self.posicion==0:
            return ""
        return "["  + str(self.posicion) + "]"
    
    def __str__(self):
        op      =   self.descripcion_operacion()
        desc_pos=   self.descripcion_posicion()
        nombre  =   self.registro.get_nombre()
        return nombre + desc_pos + op
            
    def procesar():
        pass

"""Esta microinstrucción vuelca el contenido de algo en el bus/buses"""
class MicroinstruccionVolcar(Microinstruccion):

    registro=None
    
    
    def __init__(self, suscriptor, num_salida=0):
        self.registro=suscriptor
        self.posicion=num_salida
        self.descripcion="OUT"
    def procesar(self):
        self.registro.activar_salida(self.num_salida)
    def get_dato_volcado(self):
        return self.registro.get_valor_salida(self.num_salida)
    
        
class MicroinstruccionLeer(Microinstruccion):
    
    registro=None
    
    def __init__(self, suscriptor, num_entrada=0):
        self.registro=suscriptor
        self.posicion=num_entrada
        self.descripcion="IN"
    def procesar(self):
        self.registro.activar_entrada(self.num_entrada)
        
        
        
class MicroinstruccionCargaValor(Microinstruccion):
    valor=0
    entrada=0
    descripcion="IN"
    registro=None
    def __init__(self, suscriptor, num_entrada, valor):
        self.registro=suscriptor
        self.entrada=num_entrada
        self.valor=valor
        
    def procesar(self):
        self.registro.activar_entrada(self.entrada)
        self.set_valor_entrada(self.valor)
        
    def __str__(self):
        op      =   self.descripcion_operacion()
        desc_pos=   self.descripcion_posicion()
        nombre  =   self.registro.get_nombre()
        carga   =   "<---" + str(self.valor)
        return nombre + desc_pos + op + carga
    
        
        
"""Una instrucción es un conjunto de órdenes sobre los elementos de la ruta de datos.
Unas órdenes serán de entrada y otras de salida, pero para garantizar que todo funcione
bien, primero se activan las salidas y luego la entradas"""
class Instruccion:
    microinstrucciones=[]
    def anadir_microinstruccion(self, microinstruccion):
        self.microinstrucciones.append(microinstruccion)
    def procesar(self):
        #Se procesan primero las salidas
        salidas=[mi.descripcion for mi in self.microinstrucciones if mi.descripcion=="OUT"]
        #print salidas
        if len(salidas)!=1:
            raise Exception("No puede haber mas de un out por instruccion")
        
    def __str__(self):
        descripciones=[str(x) for x in self.microinstrucciones]
        return " - ".join(descripciones)

class TestElemento(unittest.TestCase):
    def setUp(self):
        self.elemento=Suscriptor(1,1)
    def testEntrada(self):
        valor_escrito=20
        self.elemento.activar_entrada(0)
        self.elemento.set_valor_entrada(valor_escrito, 0)
        valor_leido=self.elemento.get_valor_entrada(0)
        self.assertEqual( valor_escrito, valor_leido)
    
    def testEntrada1(self):
        otro=Suscriptor(2, 2)
        valor_escrito=11
        num_pos=1
        otro.activar_entrada(num_pos)
        otro.set_valor_entrada(valor_escrito, num_pos)
        valor_leido=otro.get_valor_entrada(num_pos)
        self.assertEqual( valor_escrito, valor_leido )
        
    def testActivaciones(self):
        suscriptor=Suscriptor(1,1)
        suscriptor.activar_entrada(0)
        estado=suscriptor.entrada_esta_activada()
        self.assertEquals(estado, True)
        
    def testRegistro1(self):
        valor_escrito=8
        
        registro=Registro()
        registro.activar_entrada(0)
        registro.set_valor_entrada(8)
        registro.activar_salida(0)
        registro.procesar()
        
        valor_leido=registro.get_valor_salida()
        self.assertEqual(valor_escrito, valor_leido)
        
    def testRegistro2(self):
        valor_escrito=12
        
        registro=Registro()
        registro.activar_entrada(0)
        registro.set_valor_entrada(8)
        registro.activar_salida(0)
        registro.procesar()
        
        valor_leido=registro.get_valor_salida()
        self.assertNotEqual(valor_escrito, valor_leido)
      
    def testRegistroConRegistro(self):
        r1=Registro()
        r2=Registro()
        valor_entrada=20
        r1.anadirSuscriptor(0, r2, 0)
        r1.activar_entrada()
        r1.set_valor_entrada( valor_entrada )
        r2.activar_entrada()
        r1.procesar()
        valor=r2.get_valor_entrada()
        self.assertEqual(valor, valor_entrada)
        
    def testALU1(self):
        valor1  = 10
        valor2  = 2 
        suma    = valor1 + valor2
        
        alu=ALU(2, 1)
        alu.activar_entrada(0)
        alu.activar_entrada(1)
        alu.set_valor_entrada(valor1, 0)
        alu.set_valor_entrada(valor2, 1)
        alu.seleccionar_operacion(ALU_SUMA)
        
        alu.activar_salida()
        alu.procesar()
        
        suma_calculada=alu.get_valor_salida()
        self.assertEqual(suma, suma_calculada)
        
    def testALU2(self):
        valor1  = 10
        valor2  = 5 
        producto= valor1 * valor2
        
        alu=ALU(2, 1)
        alu.activar_entrada(0)
        alu.activar_entrada(1)
        alu.set_valor_entrada(valor1, 0)
        alu.set_valor_entrada(valor2, 1)
        alu.seleccionar_operacion(ALU_MULTI)
        
        alu.activar_salida()
        alu.procesar()
        
        producto_calculado=alu.get_valor_salida()
        self.assertEqual(producto, producto_calculado)
        
    def testALU3(self):
        valor1  = 10
        valor2  = 2
        valor3  = 5
        resta    = valor1 - valor2 - valor3
        
        alu=ALU(3, 1)
        alu.activar_entrada(0)
        alu.activar_entrada(1)
        alu.activar_entrada(2)
        alu.set_valor_entrada(valor1, 0)
        alu.set_valor_entrada(valor2, 1)
        alu.set_valor_entrada(valor3, 2)
        alu.seleccionar_operacion(ALU_RESTA)
        
        alu.activar_salida()
        alu.procesar()
        
        resta_calculada=alu.get_valor_salida()
        self.assertEqual(resta, resta_calculada)
    def testMemoria(self):
        memoria=Memoria(1, 2000)
        memoria.activar_entrada()
        memoria.set_valor_entrada(21)
        valor=memoria.get_valor_entrada()
        self.assertEqual(valor,21)
        
        memoria.set_mar(200)
        leido=memoria.get_mar()
        self.assertEquals(leido, 200)
    
    def testMicroinstruccionLeer(self):
        r1=Registro()
        r1.set_nombre("R1")
        mi1=MicroinstruccionVolcar(r1, 0)
        #print mi1
        
        r2=Registro()
        r2.set_nombre("R2")
        mi2=MicroinstruccionLeer(r2, 0)
        #print mi2
        
        i=Instruccion()
        i.anadir_microinstruccion(mi1)
        i.anadir_microinstruccion(mi2)
        
        valor1  = 10
        valor2  = 2
        valor3  = 5
        alu=ALU(3, 1)
        alu.set_nombre("ALU")
        alu.activar_entrada(0)
        alu.activar_entrada(1)
        alu.activar_entrada(2)
        alu.set_valor_entrada(valor1, 0)
        alu.set_valor_entrada(valor2, 1)
        alu.set_valor_entrada(valor3, 2)
        mi3=MicroinstruccionLeer(alu, 2)
        #print mi3
        i.anadir_microinstruccion(mi3)
        #print i
        i.procesar()
    def testMicroinstruccionCargaValor(self):
        r1=Registro()
        r1.set_nombre("R1")
        mi1=MicroinstruccionCargaValor(r1, 0, 25)
        #print mi1
        print r1
        
#if __name__ == '__main__':
#    unittest.main()


