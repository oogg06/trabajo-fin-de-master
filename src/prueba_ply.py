#!/usr/bin/python
# coding=utf-8


from ply import lex


tokens=("LET", "VARIABLE", "NUMERO", "IGUAL")



def t_LET(t):
    r'LET'
    return t
def t_IGUAL(t):
    r'='
    return t
def t_VARIABLE(t):
    r'[A-Z]+'
    return t
def t_NUMERO(t):
    r'[0-9]+'
    return t
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")
    
datos="""
    LET AX=20
"""


t_ignore = " \t"
lexer=lex.lex()
#lexer.input(datos)
#while True:
#    tok=lexer.token()
#    if not tok:
#        break
#    print tok

precedence=()
def p_lista(p):
    'lista : asignacion'
    print "Lista"
    
def p_asignacion(p):
    'asignacion : LET VARIABLE IGUAL NUMERO'
    print "Asignacion"
    
def p_error(p):
    print("Error de parseo")
    print "-->"
    print p
    print "Fin de error"
    

from ply import yacc
parser = yacc.yacc()

parser.parse(datos)
