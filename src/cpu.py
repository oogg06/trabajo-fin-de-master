#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame
from vm import Registro

class RegistroGrafico(Registro):
    def set_valor_entrada(self, valor, num_pos=0):
        super(Registro, self).set_valor_entrada(valor)
        print "Llamada"

class Simulador(object):
    blanco=(240, 240, 220)
    pantalla=None
    fuente=None
    def __init__(self, ancho, alto):
        pygame.init()
        tamano=(ancho, alto)
        self.pantalla   = pygame.display.set_mode ( tamano )
        self.fuente     = pygame.font.Font(None, 25)
        
        pygame.display.set_caption ("Simulador VM")
        
    def determinar_coordenadas_caja(self, x, y, texto):
        """Dadas una coordenada X e Y donde se empezará a dibujar un registro,
        se devuelve una tupla donde aparecerán las coordenadas del rectángulo
        y las del texto"""
        
    
    def ejecutar(self, fps):
        r=RegistroGrafico()
        """Se puede indicar la velocidad en Frames Por Segundo"""
        terminado   =False
        reloj       = pygame.time.Clock()
        
        while not terminado:
            for evento in pygame.event.get():
                if evento.type == pygame.QUIT:
                    terminado = True
                #Fin de comprobación de eventos
            #Fin del for
            r.set_valor_entrada(1)
            self.pantalla.fill ( self.blanco )
            pygame.display.flip()
            reloj.tick ( fps )
        #Fin del while
        pygame.quit()
    #Fin del método ejecutar




if __name__ == '__main__':
    simulador=Simulador(640, 480)
    simulador.ejecutar(20)