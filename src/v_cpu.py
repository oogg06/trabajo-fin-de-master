#!/usr/bin/env python
# coding=utf-8

import random
from Cheetah.Template import Template

def rellenar_archivo_plantilla(nombre_archivo_plantilla, nombre_archivo_destino, datos):
    archivo_plantilla=open(nombre_archivo_plantilla, "r")
    lineas=archivo_plantilla.read()
    
    #Si el archivo plantilla está en formato unicode se convierte a ascii
    lineas=lineas.decode("utf-8")
    plantilla=Template(lineas, datos)
    
    archivo=open(nombre_archivo_destino, "w")
    archivo.write(str(plantilla))
    archivo.close()


class Registro(object):
    nombre=""
    contenido=0
    def __init__(self, nombre):
        self.nombre=nombre
    def limpiar(self):
        self.contenido=0
    def set_valor(self, valor):
        self.contenido=valor
    def get_valor(self):
        return self.contenido
    def __repr__(self):
        return self.nombre
        
class Memoria(object):
    tamanio=0
    contenido=[]
    ancho_de_palabra=16
    def __init__(self, tamanio):
        self.tamanio=tamanio
        contenido=[0 for i in range(tamanio)]
    def llenar_de_basura():
        contenido=[random.randint(0, (2**self.ancho_de_palabra)-1) \
                   for i in range(tamanio)]
    def leer_posicion(self, pos):
        return contenido[pos]
    def escribir_posicion(self, pos, valor):
        self.contenido[pos]=valor
        
class Operacion(object):
    """Una operación afecta a un solo elemento de la ruta de datos. Normalmente
    una operacion consiste en volcar el contenido de algo en el bus, leer
    el contenido del bus y meterlo en algún registro, leer o escribir de memoria
    o cosas similares"""
    def __repr__(self):
        return ""

class OperacionConRegistro(Operacion):
    nombre_registro=""
    def __init__(self, nombre):
        self.nombre_registro=nombre
    

class Operando(object):
    valor=0
    def set_registro(self, registro):
        pass
    def set_memoria(self, memoria):
        pass
    def set_valor(self, valor):
        self.valor=valor
    def get_valor(self):
        return self.valor
    def __repr__(self):
        return ""
    def get_token(self):
        pass
    
    def repr_vm(self):
        pass

class OperandoValorAbsoluto(Operando):
    """Operando absoluto en una instrucción del tipo LOAD AX, 42"""
    valor="valor"
    nombre_parametro=""
    def set_registro(self, registro):
        pass
    def set_valor(self, valor):
        self.valor=valor
    def get_valor(self):
        return self.valor
    def __repr__(self):
        return str(self.valor)
    def codigo(self):
        return str(self.valor)
    def get_token(self):
        return "NUMERO"
    def repr_vm(self):
        return "self.leer_word()"
    
class OperandoValorRegistro(Operando):
    registro=None
    def set_registro(self, objeto_registro):
        self.registro=objeto_registro
    def set_valor(self, valor):
        self.registro.set_valor(valor)
    def get_valor(self):
        return self.registro.get_valor()
    def __repr__(self):
        return self.registro.__repr__()
    def codigo(self):
        return "self.get_valor_registro('" + self.registro.nombre +"')"
    def get_token(self):
        return self.__repr__()
    def repr_vm(self):
        return self.codigo()

class OperandoIndirectoRegistro(Operando):
    """Para operaciones del tipo LOAD AX, [BX]. En este caso, debe
    ser posible recuperar el contenido del registro BX, ir a esa posición
    de memoria y obtener dicho contenido, para cargarlo en AX"""
    registro=None
    memoria=None
    def set_registro(self, objeto_registro):
        self.registro=objeto_registro
    def set_memoria(self, memoria):
        self.memoria=memoria
    def get_valor(self):
        posicion=self.registro.get_valor()
        return memoria.leer_pos(posicion)
    def set_valor(self, valor):
        posicion=self.registro.get_valor()
        memoria.escribir_pos(valor)
    def __repr__(self):
        return "["+ self.registro.__repr__() +"]"
    def get_token(self):
        return "CORCHETE_ABIERTO "+ self.registro.__repr__() + " CORCHETE_CERRADO"
    def codigo(self):
        return "self.get_contenido_posicion_memoria('"+self.registro.nombre+"')"
    def repr_vm(self):
        return self.codigo()
    
class Instruccion:
    def ejecutar():
        pass
    def set_bytecode(self, numero):
        self.bytecode=numero
        
class InstruccionSuma(Instruccion):
    def __repr__(self):
        return "ADD"
    def ejecutar(op1, op2):
        """Una posible instrucción virtual. Dados dos operandos,
        suma sus valores"""
        return op1.get_valor() + op2.get_valor()
    def codigo(self):
        return "self.sumar"
        
class InstruccionResta(Instruccion):
    def __repr__(self):
        return "SUB"
    def ejecutar(op1, op2):
        return op1.get_valor() - op2.get_valor()
    def codigo(self):
        return "self.restar"

class InstruccionMulti(Instruccion):
    def __repr__(self):
        return "MUL"
    def ejecutar(op1, op2):
        return op1.get_valor() * op2.get_valor()
    def codigo(self):
        return "self.multiplicar"

class InstruccionDiv(Instruccion):
    def __repr__(self):
        return "DIV"
    def ejecutar(op1, op2):
        return op1.get_valor() / op2.get_valor()
    def codigo(self):
        return "self.dividir"

class InstruccionAnd(Instruccion):
    def __repr__(self):
        return "AND"
    def ejecutar(op1, op2):
        return op1.get_valor() & op2.get_valor()
    def codigo(self):
        return "self.hacer_and"
class InstruccionXor(Instruccion):
    def __repr__(self):
        return "XOR"
    def xor_operandos(op1, op2):
        return op1.get_valor() ^ op2.get_valor()
    def codigo(self):
        return "self.hacer_xor"
class InstruccionOr(Instruccion):
    def __repr__(self):
        return "OR"
    def or_operandos(op1, op2):
        return op1.get_valor() | op2.get_valor()
    def codigo(self):
        return "self.hacer_or"
    
        

class InstruccionAL(Instruccion):
    operandos=[]
    fn=None
    instruccion=None
    operando_resultado=None
    def __init__(self):
        self.operandos=[]
    def anadir_operando(self, operando):
        self.operandos.append(operando)
        
    def set_operando_resultado(self, operando):
        self.operando_resultado=operando
    def get_operando_resultado(self):
        return self.operando_resultado
    def get_operandos(self):
        return self.operandos
    def get_instruccion(self, instruccion):
        return self.instruccion
    
    def destino_operando_indirecto(self):
        pass
    def get_codigo_operandos(self, op_extra):
        codigos=[]
        codigos.append(op_extra.repr_vm())
        for op in self.operandos:
            codigos.append(op.repr_vm())
        x=",".join(codigos)
        return "["+x+"]"
    def get_codigo(self):
        codigo=self.instruccion.codigo()
        op_resultado=self.operando_resultado.registro.nombre
        
        operandos=self.get_codigo_operandos(self.operando_resultado)
        #Si resulta que el operando resultado usa direccionamiento
        #indirecto, se debe encerrar todo en un set_contenido_posicion_memoria
        if isinstance(self.operando_resultado, OperandoIndirectoRegistro ):
            return "self.set_contenido_posicion_memoria('"+op_resultado+"',"\
            +" reduce("+codigo+","+operandos+"))"
        else:
            codigo_registro="self.cargar_valor_en_registro('"+op_resultado+"',"
            return codigo_registro + " reduce("+codigo+","+operandos+"))"
        
    #Cada regla de una gramática lleva un número para hacer cosas como
    #'instruccion0', 'instruccion1', ... 'instruccion20'...
    def get_regla_gramatica(self, num_produccion):
        """Genera codigo PLY para una producción perteneciente a una
        gramática BNF."""
        token=self.instruccion.__repr__()
        token+=" " +self.operando_resultado.get_token()
        for op in self.operandos:
            if str(op)=="valor":
                token=token+" NUMERO"
            else:
                token=token+" "+op.get_token()
        print "Regla:"+token
            
    def ordenar_parametros(self):
        i=0
        parametros=[]
        for op in self.operandos:
            if isinstance(op, OperandoValorAbsoluto):
                op.nombre_parametro="valor"+str(i)
                parametros.append(op.nombre_parametro)
                op.set_valor("valor"+str(i))
                i=i+1
        return ",".join(parametros)
    def get_metodo(self, numero):
        parametros=self.ordenar_parametros()
        print "Parametros:"+parametros
        if parametros!="":
            parametros=","+parametros
        nombre_metodo="instruccion"+str(numero)
        #codigo="\tdef instruccion"+str(numero)+"(self"+parametros+"):\n"
        #codigo+="\t\t"+self.get_codigo()
        codigo=self.get_codigo()
        return (nombre_metodo, parametros,codigo)
    
    def set_instruccion(self, instruccion):
        """Dada una función se aplica dicha funcion al conjunto
        de operandos"""
        self.fn=instruccion.ejecutar
        self.instruccion=instruccion
    def ejecutar(self):
        resultado=reduce(self.fn, self.operandos)
        self.operando_resultado.set_valor(resultado)
        
    def __str__(self):
        operacion=self.instruccion.__repr__()
        lista_operandos=[operando.__repr__() for operando in self.operandos]
        cadena_operandos=" ".join(lista_operandos)
        return operacion + " " +self.operando_resultado.__repr__() \
            +  " " + cadena_operandos
    
class GeneradorInstruccionesAL(object):
    """Este objeto genera instrucciones aritmético lógicas al azar"""
    operandos=[]
    instruccion=None    
    posibles_operandos=[OperandoValorAbsoluto, OperandoValorRegistro,
                        OperandoIndirectoRegistro]
    posibles_instrucciones=[InstruccionSuma, InstruccionResta,
                            InstruccionMulti, InstruccionDiv,
                            InstruccionAnd, InstruccionOr, InstruccionXor]
    posibles_registros=[]
    
    
    def limpiar(self):
        self.operandos=[]
        self.instruccion=None
        del self.instruccion
    def generar_instruccion(self, num_operandos=2):
        self.limpiar()
        self.instruccion=InstruccionAL()
        
        i=self.elegir_instruccion()
        self.instruccion.set_instruccion(i)
        
        op=self.elegir_operando_resultado()
        self.instruccion.set_operando_resultado(op)
        for i in range(num_operandos):
            op=self.elegir_operando_al_azar()
            self.instruccion.anadir_operando(op)
            
        return self.instruccion
    
    def set_posibles_registros(self, posibles_registros):
        self.posibles_registros=posibles_registros
        
    
    def elegir_registro_al_azar(self):
        num_azar=random.randint(0, len(self.posibles_registros)-1)
        return self.posibles_registros[num_azar]
        
    def elegir_operando(self, min, max):
        num_azar=random.randint(min, max)
        operando=self.posibles_operandos[num_azar]()
        registro=self.elegir_registro_al_azar()
        operando.set_registro(registro)
        return operando
    def elegir_operando_al_azar(self):
        return self.elegir_operando(0, len(self.posibles_operandos)-1)
    def elegir_operando_resultado(self):
        return self.elegir_operando(1, len(self.posibles_operandos)-1)
        
    def elegir_instruccion(self):
        num_azar=random.randint(0, len(self.posibles_instrucciones)-1)
        return self.posibles_instrucciones[num_azar]() 
    
    
class CPU(object):
    registros=[]
    num_registros=0
    registro_pc={"PC":0}
    memoria=None
    alu=None
    def __init__(self, num_registros, tam_memoria_en_bytes, num_entradas_alu):
        self.num_registros=num_registros
        self.registros=[Registro("R"+str(i)) for i in range(num_registros)]
        self.memoria=Memoria( tam_memoria_en_bytes )
    
    def interrumpir(self):
        print "INTERRUPCION"
    def ejecutar (self, instruccion ):
        if instruccion.__class__=="InstruccionAL":
            self.ejecutar_instruccion_aritmetico_logica(instruccion)
    
    def cargar_valor_en_registro (self, valor, nombre_registro):
        for i in range(self.num_registros):
            reg=self.registros[i]
            if (reg.nombre==nombre_registro):
                reg.set_valor(valor)
    
    def ejecutar_instruccion_aritmetico_logica(self, instruccion):
        print "OK"
        operandos=instruccion.get_operandos()
        operando_resultado=instruccion.get_operando_resultado()
        for op in operandos:
            print "-->"+str(op.get_valor())
            self.interrumpir()
            
            
class CodigoInstruccion(object):
    def __init__(self, nombre, parametros, codigo, bytecode):
        self.nombre=nombre
        self.parametros=parametros
        self.codigo=codigo
        self.bytecode=bytecode
    def get_nombre(self):
        return self.nombre
    def get_parametros(self):
        return self.parametros
    def get_codigo(self):
        return self.codigo
    def get_bytecode(self):
        return self.bytecode
    def set_operandos(self, operandos):
        self.operandos=operandos
    def get_operandos(self, operados):
        return self.operandos
    def repr_operandos(self):
        lista=[]
        for op in self.operandos:
            cad=op.repr_vm()
            print "Repr:"
            print cad
            lista.append(op.repr_vm())
        if len(lista)==0:
            return lista[0]
        else:
            return ",".join(lista)
cpu=CPU(8, 65536, 2)
g=GeneradorInstruccionesAL()
g.set_posibles_registros(cpu.registros)
lista=[]
datos=dict()
for x in range(5):
    i=g.generar_instruccion()
    i.set_bytecode(x)
    print i
    print i.get_regla_gramatica(x)
    (nombre_metodo, parametros, codigo)=i.get_metodo(x)
    c=CodigoInstruccion(nombre_metodo, parametros, codigo, x)
    c.set_operandos(i.get_operandos())
    print codigo
    lista.append(c)

datos["lista"]=lista
rellenar_archivo_plantilla("plantilla_vm.py", "vm00.py", datos)