\chapter{Propuesta de investigación}
\section{Definición}
En las secciones anteriores de este trabajo se ha evaluado el estado del arte en dos campos: el diseño de juegos de instrucciones y las arquitecturas de máquinas virtuales. A continuación se analizan los principales puntos sobre los que se sustentará la propuesta de investigación aquí formulada.

\begin{itemize}
\item{Por un lado se ha examinado la amplia variedad de opciones en máquinas virtuales para dispositivos empotrados así como las decisiones de diseño de muchas de ellas. Dado que en esta clase de dispositivos empotrados la variabilidad es mucho mayor que en el hardware típico de escritorio es posible afirmar que la utilización de máquinas virtuales ofrece un beneficio aún mayor para los programadores.}
\item{Dadas las restricciones bajo las que operan las WSN y los microcontroladores cualquier mejora que pueda ofrecerse a los programadores puede suponer un impacto muy elevado en la portabilidad, rendimiento o consumo de energía de las aplicaciones.}
\item{El diseño de juegos de instrucciones es una labor compleja: el altísimo número de posibilidades a explorar tal vez podría analizarse mediante algoritmos que obtengan resultados más rápidamente y sobre todo con mejores resultados.}
\item{En este sentido las técnicas de soft-computing se han mostrado muy útiles a la hora de obtener buenas soluciones en problemas de complejidad muy elevada o con un abanico de posibilidades demasiado amplio de analizar incluso para un programa informático.}
\item{Las máquinas virtuales para dispositivos empotrados pueden tener que enfrentarse a tareas muy diversas con requisitos muy dispares. En este contexto el diseño de máquinas nuevas supone que la experiencia acumulada en otros campos podría no servir para nada: es decir, el diseño de una máquina virtual donde el juego de instrucciones intente minimizar el consumo de energía de los programas podría no servir para mucho al intentar diseñar otra máquina distinta donde se busque ofrecer niveles de abstracción mayores.}
\item{Estos requisitos podría convertirse en parámetros de los algoritmos de softcomputing que exploran las posibilidades para así optimizar los resultados en base a las necesidades que puedan tener los usuarios de la máquina virtual.}
\end{itemize}

En base a los puntos aquí analizados, se formula la siguiente tesis:

\begin{quote}
{\sl Utilizando técnicas de inteligencia artificial es posible diseñar de forma automatizada y parametrizable máquinas virtuales cuyos juegos de instrucciones ofrezcan valores óptimos para cualquier métrica que los diseñadores deseen.}
\end{quote}

\section{Arquitectura}

La estructura general de la solución podría ser algo similar a lo mostrado en la figura:

\begin{figure}[h]
\begin{center}
\includegraphics[width=\linewidth]{imagenes/EsquemaSolucion.png} 
\caption{Esquema general de la solución}
\label{fig:solucion}
\end{center}
\end{figure}


La primera etapa de la solución pasaría por desarrollar un generador de juegos de instrucciones, que tomando como datos de entrada las métricas a optimizar y la especificación del hardware diese como resultado la descripción de un juego de instrucciones.

En una segunda etapa, sería necesario crear un compilador que tomando como entrada la descripción de un juego de instrucciones y programas de ejemplo fuera capaz de generar los bytecodes correspondientes.

Por último, un tercer programa debería ejecutar dichos programas y recolectar las estadísticas necesarias que permitan evaluar la bondad del juego de instrucciones generado.

Utilizando como resultado los valores generados por la tercera etapa, podrían volver a insertarse estos datos como entrada de la primera para determinar si se debe corregir la actuación del sistema en su conjunto.

\section{Primera etapa}
\subsection{Problema 1: métricas}
En este punto surgen dos problemas que se deberán resolver. El primero está relacionado con las métricas que se desean optimizar ya que de hecho el enunciado del propio problema ya plantea la primera dificultad: ¿como se puede medir si un juego de instrucciones es bueno o no?

Probablemente, estas métricas tengan que ser diferentes en función del objetivo de la máquina virtual. 
\begin{itemize}
\item{Si se pretende generar una máquina virtual cuyo objetivo sea el rendimiento,  probablemente deba buscarse un juego de instrucciones que maneje muchos registros y que minimice las transferencias de datos a memoria.}
\item{Si se busca la eficiencia energética interesará buscar instrucciones virtuales cuyo consumo sea menor.}
\item{Si el hardware objetivo dispone de poca RAM quizá lo mejor sea crear instrucciones de muy alto nivel con vistas a reducir el tamaño de los programas. }
\end{itemize}

En suma, será necesario un estudio previo de las necesidades de las distintas máquinas virtuales y de las posibles métricas que pudieran ser útiles.
\subsection{Problema 2: especificación del hardware}
Con vistas a generar un compilador que genere código para la máquina virtual será necesario describir de alguna forma el hardware destino. En este sentido, y como se ha visto anteriormente, ya existe trabajo realizado en este aspecto, por lo que no es necesario partir de cero para resolver este problema.

\subsection{Problema 3: técnica de exploración}
El número de posibilidades a explorar a la hora de generar instrucciones es muy elevado por lo que se necesitarán examinar qué técnicas de softcomputing pueden ser susceptibles de producir buenos resultados en tiempos aceptables.

Se ha considerado la posibilidad de utilizar algoritmos genéticos para generar conjuntos de instrucciones. A modo de ejemplo, tres posibles formatos de instrucción de un ancho de 16 bits podrían ser los mostrados en el cuadro \ref{genes}. Así, una instrucción puede evolucionar a base de modificar la utilidad de cada uno de estos bits (que actúa como genes) y despues calcular la bondad de la mutación mediante una función de las métricas comentadas anteriormente y decidir así si el cambio en un gen es aceptable.
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
15 & 14 & 13 & 12 & 11 & 10 & 9 & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1 & 0\\
\hline
C & C & C & C & C & C & C & C & OP1 & OP1 & OP1 & OP1 & OP2 & OP2 & OP2 & OP2 \\
\hline
C & C & C & C & C & C & C & C & C & C & OP1 & OP1 & OP1 & OP2 & OP2 & OP2 \\
\hline
C & C & C & C & C & C & C & OP1 & OP1 & OP1 & OP2 & OP2 & OP2 & OP3 & OP3 & OP3 \\
\hline

\end{tabular}
\label{genes}
\caption{Ejemplos de bits actuando como genes}
\end{table}

La explicación de dicha tabla es la siguiente: si C es un bit utilizado para el código de operación, OP1 es un bit para representar el operando 1, y OP2 es un bit del operando 2 el primer formato de instrucción tiene 8 bits para representar operaciones (con 256 posibles instrucciones) y cada instrucción podría manejar dos operandos a elegir de, quizá, 16 registros.

El segundo sistema tendría más posibles operaciones pero tendría menos operandos donde elegir, lo que tal vez sea útil si desea tener muchas instrucciones o de muy alto nivel.

El tercero utilizaría tres operandos a elegir de 8 posibles. Esto daría mucha flexibilidad al programador, sin embargo el número de posibles instrucciones se reduce a 128 posibilidades.

Si se utiliza como ejemplo de mutación el cambio de un bit para que pase de ser C a OP1 o viceversa puede estudiarse despues si una mutación es apropiada o no.

\subsubsection{Problema adicional}
Por otro lado debe considerarse el problema siguiente: tradicionalmente los algoritmos genéticos han utilizado individuos que mutan de alguna manera aleatoria. Si la mutación da un valor mejor de su función de bondad y este valor supera cierto umbral, se asume que este individuo pasa a la siguiente generación mientras que otros son descartados.

En el caso que aquí se estudia hay un problema al tener que observar {\sl <<grupos de individuos>>} en los cuales se pueden producir situaciones difíciles de evaluar. Supóngase que se tienen dos conjuntos de instrucciones en los cuales hay una instrucción que mejora mucho la bondad global del conjunto. Se tendría que estudiar si se debería darse por buena esa población frente a otras poblaciones, así como analizar la forma en que se tratan poblaciones distintas dependiendo de si han llegado a un mismo valor de bondad a través de valores medios parecidos o muy dispares.

En la figura \ref{fig:comparativa} se ilustra dicho problema. El juego de instrucciones de la izquierda tiene tres instrucciones, donde cada una de ellas contribuye a la bondad del conjunto en factores pequeños, dando como métrica resultado un valor de 4. Por el contrario el juego de la derecha ofrece un resultado muy superior, sin embargo tal vez debería reconsiderarse el elegir este conjunto al existir la posibilidad de que lleve por una rama de la búsqueda poco prometedora. 

\begin{figure}[h]
\begin{center}
\includegraphics[width=\linewidth]{imagenes/ComparativaPoblacionesInstrucciones.png} 
\caption{Comparativa entre juegos de instrucciones	}
\label{fig:comparativa}
\end{center}
\end{figure}


\section{Segunda etapa}
\subsection{Problema 4: compilador}
El desarrollo de un compilador que pueda generar código para diversos juegos de instrucciones plantea serias dificultades:
\begin{itemize}
\item {Para empezar se debería seleccionar un lenguaje de alto nivel en el cual se escriban los programas de ejemplo.}
	\begin{itemize}
		\item{Si se elige un lenguaje de alto nivel conocido se puede aprovechar el conocimiento que ya tengan los programadores, sin embargo, los lenguajes más populares (C, Java\ldots) suelen tener una sintaxis compleja que complica la creación del compilador.}
		\item{Si se diseña un lenguaje a medida el compilador puede simplificarse cuanto se desee y hacerlo crecer según se vayan obteniendo resultados, lo que evidentemente es más sencillo, aunque se pierda el conocimiento de los programadores.}
		\item{Por último se tiene la opción de escoger un subconjunto de un lenguaje ya conocido, aunque también habrá que tener en cuenta que será necesario estudiar detenidamente qué partes elegir y también de qué lenguaje tomarlas.}
	\end{itemize}
\item{Al ser un compilador genérico es muy posible que se complique sobremanera la generación de código optimizado, ya que a menudo tales optimizaciones pueden aplicarse por parte de programadores de compiladores que conocen muy bien la arquitectura del juego de instrucciones destino.}
\item{Tampoco se ha hablado hasta ahora de la arquitectura de la máquina virtual destino: por ejemplo, una elección absolutamente básica es el mecanismo básico de almacenamiento. En este sentido, la bibliografía incluye estudios sobre las ventajas de unos sobre otros~\cite{showdown}. Una arquitectura basada en pila permite instrucciones más compactas y simplifica la creación del compilador, ya que por ejemplo, no es necesario decidir qué variables se asignan a qué registros. Sin embargo, las arquitecturas basadas en registros parecen ofrecer más rendimiento neto basándose en que se necesitan ejecutar menos instrucciones lo que significa ejecutar menos trabajo y además reducir la probabilidad de errores de caché en los microprocesadores físicos.}
\end{itemize}
\subsection{Problema 5: programas de ejemplo}
En las diferentes propuestas sobre máquinas virtuales se hace hincapie en la dificultad de elegir programas de ejemplo que actúen como valores representativos de las tareas típicas que se programarán. A modo de ejemplo, algunos de los programas elegidos en la bibliografía son los siguientes:
\begin{itemize}
	\item{En~\cite{case} se han elegido (entre otros) un compresor, un solucionador de ecuaciones basado en el método de Montecarlo, un programa de {\sl raytracing}, una búsqueda Alfa-Beta, o un codificador de audio MP3.}
	\item{En~\cite{mate} se usan programas que alteran LEDs, o un programa que comprueba un sensor continuamente y avisa a otro nodo si el valor cambia. Este último se utiliza también en~\cite{tinyreef}.}
	\item{En \cite{esterelvm} se utiliza un programa que recorre un grafo, otro que hace un recuento de ciertos valores calculando estadísticas, y otro que genera 100 valores al azar para luego procesarlos.}
\end{itemize}
Una posibilidad para hacer las pruebas iniciales sería diseñar un programa relativamente simple escrito en un lenguaje de alto nivel que fuera sencillo de pasar a los distintos bytecodes tal vez mediante técnicas como la transformación de programas.

Por ejemplo, el compilador GCC~\cite{gcc}, utiliza un mecanismo de compilación basado en un lenguaje intermedio denominado RTL que se utiliza para poder hacer que el compilador sea fácilmente adaptable a diversos lenguajes. Este mecanismo podría ser imitado con el fin de facilitar la tarea de compilar programas en lenguajes de alto nivel a los bytecodes generados por este sistema.

\begin{figure}[h]
\begin{center}
\includegraphics[width=\linewidth]{imagenes/gcc.jpg} 
\caption{Estructura de GCC (imagen tomada de http://wikimedia.org)}
\label{fig:estructuragcc}
\end{center}
\end{figure}

\section{Tercera etapa}
\subsection{Problema 6: evaluador}
El diseño de un programa que evalúe los juegos de instrucciones puede abordarse de distintas formas, cada una de las cuales puede presentar sus propios retos y que además presentan interrelaciones con otros problemas de otras etapas.
\begin{itemize}
	\item{¿Cuando se decide si una mutación es lo bastante buena como para pasarla a la siguiente generación?}
	\item{Si en una población aparecen varios individuos con una pequeña mejora pero uno empeora mucho ¿como va a reaccionar la función que mide la adaptación? ¿puede decidirse elegir varias pequeñas mejoras con la esperanza de que eso lleve a un buen resultado? ¿o se elige siempre aquello que mejore el resultado global? si se decide usar un valor umbral ¿qué valor será el mejor?}
	\item{¿Puede elegirse alguna técnica distinta de softcomputing que permita explorar los resultado más rápidamente? ¿pueden combinarse técnicas?}
\end{itemize}

\subsection{Problema 7: estadísticas}
Una posibilidad que puede resultar de utilidad sería el no descartar ningún juego de instrucciones y utilizar los conjuntos de resultados para analizar {\sl a posteriori} si el algoritmo funcionó correctamente. En ese caso habría que analizar lo siguiente:
\begin{itemize}
	\item{Habrá que considerar detenidamente los datos que se van a almacenar de cada población de instrucciones descartada. Algunas posibilidades son: el valor de la función de adaptación, la generación en que se descartó, el tiempo de generación, si el juego de instrucciones era completo o no, cuantos operandos podian procesar las instrucciones, si el nivel de las instrucciones descartadas era muy alto o no, etc\ldots}
	\item{El problema del tratamiento estadístico de un conjunto de datos es, de por sí, un problema conocido cuya solución es difícil de resolver. Es muy posible que sea necesario un análisis detenido si se pretende que los resultados puedan ser de utilidad, especialmente si estos valores que se obtengan se van a reintroducir a la entrada del sistema para mejorar el juego de instrucciones por medio de refinamientos sucesivos.}
\end{itemize}
\section{Resultados preliminares}
En este apartado se describen algunos resultados obtenidos en una implementación muy básica que pueda servir de infraestructura a la propuesta de investigación defendida. Utilizando el lenguaje de programación Python se ha construido una arquitectura de programas que han permitido obtener algunas conclusiones de utilidad.

\subsection{Arquitectura de una máquina virtual para dispositivos empotrados}
Dado que el propósito de este estudio es investigar los juegos de instrucciones se ha partido de una arquitectura virtual muy sencilla que pueda servir como punto de partida para la investigación. En concreto, la CPU virtual implementada muestra las siguientes características

\begin{itemize}
\item{No utilizará los bytecodes de Java.}
\item{Está basada en registros. Sin embargo, el software de simulación creado permite modificar el número de registros utilizados modificando una sola constante.}
\item{Utilizará exclusivamente operandos de 16 bits, lo que resulta un tamaño razonable para el hardware examinado en este trabajo. Sin embargo, la anchura de los operandos puede alterarse por medio de una constante.}
\item{Utiliza un tamaño de memoria reducido, en concreto 64Kb, sin embargo el tamaño de la memoria simulada puede ampliarse cambiando una constante.}
\item{No ofrece soporte para tipos de datos: la única posibilidad es el procesamiento de números positivos de 16 bits (que también puede cambiarse).}
\item{No hay soporte para la multitarea: los programas ejecutados toman el control del procesador virtual y se ejecutan todo el tiempo que lo deseen.}
\end{itemize}

\subsection{Diseño UML}
En la figura~\ref{fig:solucionprop} se ilustra el diseño UML de la solución programada. 


\begin{figure}[h]
\begin{center}
\includegraphics[width=.8\linewidth]{imagenes/Disenio.png} 
\caption{Esquema general de la solución}
\label{fig:solucionprop}
\end{center}
\end{figure}


\begin{itemize}
\item{La clase {\tt Instrucción} es la clave del diseño. Una instrucción representa de forma abstracta una operación que podrá realizar la máquina virtual. Toda instrucción va a utilizar dos representaciones concretas:
	\begin{itemize}
	\item{En primer lugar, una instrucción consistirá en una secuencia de operaciones que la máquina virtual puede ejecutar, siendo en general solo instrucciones de carga/almacenamiento, instrucciones aritmético/lógicas y de salto.}
	\item{Por otro lado, una instrucción tendrá una representación en ensamblador que un <<ensamblador abstracto>> podrá reconocer y transformar en una secuencia de bytecodes que una VM concreta podrá ejecutar.}
	\end{itemize}
}
\item{Existen clases dedicadas a la generación de instrucciones de los tres tipos. De momento, y a modo de prueba, se generan instrucciones aleatorios con un número de operandos escogido al azar entre 2 y 4 y con mecanismos de direccionamiento aleatorios seleccionados entre: direccionamiento absoluto, directo e indirecto.}
\item{Los conjuntos de instrucciones generados son procesados por un <<Generador de máquinas virtuales>> que utiliza una plantilla de máquina virtual que se rellena con las representaciones de las instrucciones generadas. Así, pueden generarse tantas máquinas virtuales como se deseen con juegos de instrucciones totalmente distintos entre ellas, además de otros parámetros adaptables como números de registros o memoria RAM disponible.}
\end{itemize}


El listado siguiente ilustra como la máquina virtual recibirá <<instrucciones>> y el mecanismo que usará para ejecutarlas.

\begin{lstlisting}[caption=Plantilla de máquina virtual]
def ejecutar_archivo_bytecodes(self, nombre_archivo):
        secuencia_bytes=open(nombre_archivo, "rb").read()
        print secuencia_bytes
        self.memoria.cargar_bytecodes(secuencia_bytes)
        
        #for $pos in $lista
        if self.leer_pc==$pos.get_bytecode():
            $pos.get_codigo()
        #end for
\end{lstlisting}
El código que se muestra entre almohadillas se rellenará {\em tras la generación de las instrucciones}. El programa generador de máquinas virtuales tomará la lista de instrucciones generadas y para cada una de ellas insertará el código correspondiente a ejecutar en términos de las operaciones elementales de la máquina virtual. A continuación se muestra un ejemplo de código real con algunas instrucciones muy sencillas representadas por los bytecodes 0 a 4:

\break

\begin{lstlisting}[caption=Un ejemplo de VM]
def ejecutar_archivo_bytecodes(self, nombre_archivo):
    secuencia_bytes=open(nombre_archivo, "rb").read()
    print secuencia_bytes
    self.memoria.cargar_bytecodes(secuencia_bytes)
        
    if self.leer_pc==0:
        self.cargar_valor_en_registro('R7', 
	reduce(self.dividir, 
	[self.get_valor_registro('R7'),
	self.get_contenido_posicion_memoria('R4'),
	self.leer_word()]))
            
    if self.leer_pc==1:
	self.set_contenido_posicion_memoria('R4', 
	reduce(self.multiplicar, 
	[self.get_contenido_posicion_memoria('R4'), 	
	self.get_contenido_posicion_memoria('R1'), 		
	self.get_contenido_posicion_memoria('R4')]))
            
    if self.leer_pc==2:
        self.set_contenido_posicion_memoria('R1', 
	reduce(self.hacer_xor,
	[self.get_contenido_posicion_memoria('R1'),
	self.get_valor_registro('R4'),
	self.get_contenido_posicion_memoria('R5')]))
            
    if self.leer_pc==3:
        self.set_contenido_posicion_memoria('R4', 
	reduce(self.multiplicar,
	[self.get_contenido_posicion_memoria('R4'),
	self.leer_word(),self.leer_word()]))
            
    if self.leer_pc==4:
        self.cargar_valor_en_registro('R4', 
	reduce(self.hacer_or,
	[self.get_valor_registro('R4'),
	self.get_contenido_posicion_memoria('R3'),
	self.get_valor_registro('R7')]))
\end{lstlisting}


Existen diversos enfoques específicos de la implementación para mejorar la eficiencia en la interpretación de los bytecodes. En especial, algunas técnicas aplicables al lenguaje C pueden hacer que un intérprete pase de procesar 200.000 instrucciones por segundo hasta aproximadamente unos 500.000. Tales mejoras no se han aplicado en el prototipo inicial y tampoco se han investigado técnicas específicas para el lenguaje utilizado (Python).



\subsection{Generación de instrucciones}
En este punto del estudio, se dispone de un generador de instrucciones que construye instrucciones al azar, con un número aleatorio de operandos a escoger entre registros y valores con direccionamiento directo e indirecto y códigos de operación sencillos tales como {\tt ADD}, {\tt JZ} o {\tt LOAD}. No se ha llegado al punto de combinar instrucciones con algoritmos genéticos o a la utilización de algoritmos de soft-computing ya que no se ha desarrollado el estudio completo de las métricas a utilizar, que resulta fundamental para este proceso. Este estudio puede ser el punto de partida de una investigación más profunda que culmine en la defensa de una tesis doctoral.

\subsection{Dificultades encontradas}

Un problema de relativa dificultad ha consistido en encontrar el mecanismo correcto para poder generar los ensambladores asociadas a las máquinas virtuales. Dado que un ensamblador es un generador de código y que se pretende ir un paso más allá y generar ensambladores ha sido necesario utilizar de manera intensiva herramientas del tipo Lex y Yacc para construir un complejo sistema de analizado de gramáticas ensamblador que permitan disponer de un sistema de programación relativamente sencillo. En cualquier caso, y si fuera necesario, todas las máquinas generadas pueden procesar un archivo hexadecimal creado con cualquier editor corriente. Para estas tareas, quizá puede decirse que la herramienta más recomendable sea con toda probabilidad PLY (Python Lex\&Yacc).

\chapter{Conclusiones y trabajo futuro}
Este apartado muestra de manera resumida las conclusiones que se han extraído a partir del estudio bibliográfico y la implementación
\section{Conclusiones}
\begin{itemize}

\item{En este trabajo se han analizado las diversas tecnologías de máquina virtual para dispositivos empotrados que existen en la actualidad. Se ha realizado una clasificación de las mismas en función de diversos criterios y para cada una se han destacado los detalles que han podido ser de utilidad para este trabajo o las características que las hacen especiales. Se han examinado las características de los dispositivos empotrados que pueden limitar la funcionalidad de una máquina virtual y se han estudiado los distintos tipos de hardware que pueden entrar en esta clasificación. De la realización de estudio bibliográfico se ha encontrado que:
}
\begin{itemize}
\item{Algunos trabajos no ofrecen mucha información sobre sus juegos de instrucciones, centrándose más bien en otras características tales como rendimiento o consumo de energía, por lo que no ha sido posible hacer un análisis pormenorizado de sus operaciones internas.}
\item{Es muy frecuente que en muchos casos tampoco sea posible descargar ninguna clase de software que permita un análisis más en profundidad de las técnicas empleadas.}
\item{En algunos casos, los autores ofrecen cifras de resultados que son muy difíciles de verificar, al no poderse obtener los datos utilizadas para emitir ciertas afirmaciones.}
\end{itemize}
\item{De entre las posibilidades para una máquina virtual se ha hecho un estudio detallado de aquellas basadas en Java y se ha mostrado las limitaciones que se han auto-impuesto los diversos autores para logran encajar en un dispositivo reducido un diseño pensado para hardware de mayor capacidad. 
}

\item{Se ha hecho un pequeño estudio de los principales algoritmos utilizados para diseñar de forma automática juegos de instrucciones. Como se ha podido ver, gran parte del trabajo existente se centra en el uso de ASIP's (microprocesadores que pueden adaptarse a aplicaciones específicas) y por supuesto a procesadores normales, mientras que la bibliografía relacionada con el diseño de instrucciones es, hasta donde ha examinado, inexistente. Todo esto permite mostrar que el diseño automatizado de juegos de instrucciones en máquinas virtuales ejecutadas en dispositivos empotrados es un campo en el que puede haber mucho margen para la mejora.
}


\item{La generación automatizada de juegos de instrucciones es una técnica que puede ofrecer un gran potencial a la hora de diseñar máquinas virtuales, especialmente si la plataforma objetivo es algún dispositivo empotrado. Las dificultades adicionales que se presentan (poca velocidad de proceso, escasez de memoria, etc\ldots) hacen que la automatización del diseño de las instrucciones se vuelva aún más atractivo frente a mecanismos típicos tales como el uso de expertos. Tareas como el uso de intuición u otras técnicas manuales para la determinación de instrucciones pueden convertir la creación de una máquina virtual en algo lento, complejo y por supuesto de menor calidad que un diseño algorítmico.
}

\item{Por otro lado, en este trabajo también se ha analizado la posibilidad de utilizar técnicas de inteligencia artificial para poder resolver mejor el problema de la exploración del espacio de soluciones. Incluso utilizando las más modernas tecnologías, determinados problemas tienen un tamaño demasiado grande por lo que las técnicas de soft-computing pueden ayudar a obtener soluciones con una calidad muy alta (aunque quizás no óptima) en un tiempo razonable.
}

\item{También se han analizado los problemas que podría tener el diseño de una solución como la propuesta en este trabajo. Aunque sin duda podrían aparecer más problemas, y quizá incluso más complejos, se ha proporcionado un análisis estructurado de los pasos que se deberían dar y las dificultades que habría que resolver para poder ofrecer un software que permitiera respaldar la propuesta de tesis que aquí se hace.
}

\end{itemize}

Finalmente, parte de este trabajo ha consistido en el desarrollo de un programa que permite generar máquinas virtuales cuyos juegos de instrucciones se escogen al azar. Un trabajo posterior que podría dar lugar a una tesis doctoral consistiría en la realización completa del software que utilizara, por ejemplo, un algoritmo genético para <<hacer evolucionar>> el conjunto de instrucciones en función de unas métricas que también deberían establecerse y por supuesto validarse.

\break

\section{Trabajo futuro}

Una posible continuación de este trabajo debería resolver los siguientes problemas

\begin{itemize}
\item{Elegir una plataforma hardware empotrada que ofrezca las herramientas de desarrollo y documentación suficiente como para facilitar las tareas de desarrollo y pruebas que se mencionan en este trabajo.}
\item{Seleccionar y validar un conjunto de métricas para diversos contextos.}
\item{Determinar que técnica de soft-computing puede ser más útil a la hora de realizar la exploración automatizada del problema.}
\item{Elegir un conjunto de programas de ejemplo que sean representativas de los usos habituales del dispositivo embebido.}
\item{Posiblemente restringir el problema a un tipo de arquitectura de máquina virtual: basada en pila o en registros.}
\item{Averiguar como hacer la transformación de programas escritos en un lenguaje de alto nivel a cualquiera de las arquitecturas de bajo nivel generadas.}
\item{Determinar la función de bondad que dictamine si una determinada máquina virtual es mejor que otra.}
\end{itemize}



