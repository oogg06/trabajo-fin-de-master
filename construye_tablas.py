#!/usr/bin/env python
# -*- coding: utf-8 -*

import sqlite3
from Cheetah.Template import Template
from time import sleep
import numpy as np

from pylab import *

#Nombre de la base de datos
DB="datos.db"
CAPTION_TABLA_ARQUITECTURAS="Clasificación por arquitectura"
NOMBRE_TABLA_ARQUITECTURAS="tabla-ssoo"
NOMBRE_GRAFICO_ARQUITECTURAS=NOMBRE_TABLA_ARQUITECTURAS+".png"
LABEL_GRAFICO_ARQUITECTURAS="arquitecturas"
CAPTION_GRAFICO_ARQUITECTURAS="División por arquitectura"


PLANTILLA_HW_TMPL="plantillas/HWDestino.tmpl"
PLANTILLA_HW_TEX="./por_hardware.tex"
CAPTION_TABLA_HW="Clasificación por hardware destino"
NOMBRE_TABLA_HW="tabla-hw"
NOMBRE_GRAFICO_HW=NOMBRE_TABLA_HW+".png"
LABEL_GRAFICO_HW="hw"
CAPTION_GRAFICO_HW="División por hardware destino"

PLANTILLA_TIPO_TMPL="plantillas/Tipo.tmpl"
PLANTILLA_TIPO_TEX="./por_tipo.tex"
CAPTION_TABLA_TIPO="Clasificación por tipo de máquina"
NOMBRE_TABLA_TIPO="tabla-tipo"
NOMBRE_GRAFICO_TIPO=NOMBRE_TABLA_TIPO+".png"
LABEL_GRAFICO_TIPO="tipo"
CAPTION_GRAFICO_TIPO="Tipos de máquinas virtuales"

PLANTILLA_ESTADISTICAS_TMPL="plantillas/estadisticas.tmpl"
PLANTILLA_ESTADISTICAS_TEX="./estadisticas.tex"
CAPTION_TABLA_ESTADISTICAS=u"Clasificación por tamaño de la máquina"
NOMBRE_TABLA_ESTADISTICAS="tabla-estadisticas"
NOMBRE_GRAFICO_ESTADISTICAS=NOMBRE_TABLA_ESTADISTICAS+".png"
LABEL_GRAFICO_ESTADISTICAS="tamanos"
CAPTION_GRAFICO_ESTADISTICAS="Tamaños de máquinas virtuales"


ANCHURA_CM_TABLAS=5
ALINEAMIENTO_TABLAS="l"

def get_encabezado_tabla():
    cadena="\\begin{table}[h]\n\\centering\n\\begin{tabular}{|l|c|}\n\\hline\n"
    return cadena

def get_fin_tabla(caption, label):
    cadena="\end{tabular}\n\\caption{"+caption+"}\n\\label{"+label+"}\n\\end{table}"
    return cadena


def ejecutar_sql(sql):
    conn=sqlite3.connect(DB)
    cursor=conn.cursor()
    cursor.execute(sql)
    filas=cursor.fetchall()
    conn.close()
    return filas

def extraer_valor_unico_sql(sql):
    filas=ejecutar_sql(sql)
    return filas[0][0]

def extraer_ids_articulos(tag):
    sql="select id_articulo from articulo_tiene_tag where tag='"+tag+"'"
    #print sql
    filas=ejecutar_sql(sql)
    return filas

def extraer_citas_articulos_por_tag(tag):
    filas=extraer_ids_articulos(tag)
    lista_refs=""
    for f in filas:
        lista_refs+="\cite{"+unicode(f[0])+"} "
    #print lista_refs
    return lista_refs

def extraer_sistemas_operativos():
    return extraer_citas_articulos_por_tag('so')

def extraer_sistemas_vmjava():
    return extraer_citas_articulos_por_tag('vmjava')
def extraer_sistemas_vmnojava():
    return extraer_citas_articulos_por_tag('vmnojava')

#Dada una lista de tags crea un vector asociado que dice cuantos articulos hay de cada
def extraer_vector_cantidades(lista_tags):
    cantidades=[]
    for tag in lista_tags:
        sql="select count(*) from articulo_tiene_tag where tag='"+tag+"'"
        #print sql
        filas=ejecutar_sql(sql)
        cantidad=extraer_valor_unico_sql(sql)
        #print filas[0][0]
        cantidades.append(cantidad)
    return cantidades

def generar_pie(vector_etiquetas, vector_cantidades, titulo, nombre_archivo):
    
    f=figure(1, figsize=(6,6))
    tupla=[]
    for i in range(0, len(vector_etiquetas)):
        tupla.append(0)
    #print "La tupla es:"+str(tuple(tupla))
    explode=tupla
    
    #print vector_etiquetas
    #print vector_cantidades
    pie(vector_cantidades, explode=explode, labels=vector_etiquetas, autopct='%1.1f%%', shadow=True)
    title(titulo, bbox={'facecolor':'0.8', 'pad':5})
    savefig(nombre_archivo)
    #Es importante limpiar lo que haya, para que la siguiente ejecución
    #no encuentre datos basura con restos de la ejecución anterior
    f.clear()
    
def generar_grafico_barras(valores, nombre_archivo):
    f=figure(1, figsize=(6,6))
    anchura_barras=.35 #Anchura de las barras
    #Para indicar la posición relativa de cada valor
    #valor[0] irá en posiciones[0]...
    posiciones=np.arange(3)
    ejes=f.add_subplot(111)
    ejes.set_xticks(posiciones+anchura_barras-0.2)
    ejes.set_xticklabels( ('Peq', 'Med', 'Grandes') )
    rectangulos=ejes.bar(posiciones, valores, anchura_barras)
    for barra in rectangulos:
        altura=barra.get_height()
        x=barra.get_x()
        anchura=barra.get_width()
        ejes.text( x+(anchura/2), 1.05*altura, '%d'%int(altura))
    savefig(nombre_archivo)
    f.clear()
    

def lista_articulos(texto_fila, funcion):
    resultado_funcion=funcion().encode('utf-8')
    caja=construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, resultado_funcion)
    texto_tabla=texto_fila+" &"+caja+"\\\\\n\\hline\n"
    return texto_tabla

def fila(texto_fila, funcion):
    return lista_articulos(texto_fila, funcion)

def lista_sistemas_operativos():
    return fila("Sistemas operativos", extraer_sistemas_operativos)

def lista_vmjava():
    return lista_articulos("Máquinas virtuales Java", extraer_sistemas_vmjava)
    
def lista_vmnojava():
    return lista_articulos("Máquinas virtuales no Java", extraer_sistemas_vmnojava)

def get_tabla_por_arquitectura():
    inicio_tabla=get_encabezado_tabla()
    ssoo=lista_sistemas_operativos()
    java=lista_vmjava()
    no_java=lista_vmnojava()
    
    fin_tabla=get_fin_tabla(CAPTION_TABLA_ARQUITECTURAS, NOMBRE_TABLA_ARQUITECTURAS)
    #print fin_tabla
    cadena=inicio_tabla+ssoo+java+no_java
    cadena+=fin_tabla
    return cadena


def crear_lista_separada(elementos, separador, simbolo_encierro_apertura, simbolo_encierro_cierre):
    total=len(elementos)
    pos=0
    cadena=""
    while (pos<total-1):
        cadena+=simbolo_encierro_apertura+elementos[pos]+simbolo_encierro_cierre+separador
        pos=pos+1   
    cadena+= simbolo_encierro_apertura + elementos[pos] + simbolo_encierro_cierre
    return cadena
    
    
def construye_parbox(alineamiento, anchura, texto):
    parbox="\\parbox["+alineamiento+"]"
    parbox+="{"+str(anchura)+"cm}"
    parbox+="{"+texto+"}"
    return parbox

def get_informe_arquitectura(etiquetas, conceptos):
    inicio_sql="select count(*) from articulo_tiene_tag where tag in "
    
    lista_tags=crear_lista_separada(etiquetas, ",", "'", "'")
    lista_tags="("+ lista_tags +")"
    sql_total=inicio_sql+lista_tags
    total_clasificacion_por_arquitectura=extraer_valor_unico_sql(sql_total)
    vector_cantidades=extraer_vector_cantidades(etiquetas)
    cadena= "Utilizando el concepto ``Arquitectura'' se han clasificado "+ \
        str(total_clasificacion_por_arquitectura)+" articulos englobandose "+\
        str(vector_cantidades[0])+" en el concepto `"+conceptos[0]+"'', "+ \
        str(vector_cantidades[1])+" en el concepto `"+conceptos[1]+"'' y "+ \
        str(vector_cantidades[2])+" en  `"+conceptos[2]+"'' resumidos en el cuadro~\\ref{"+NOMBRE_TABLA_ARQUITECTURAS+"} y mostrados en la figura~\\ref{"+LABEL_GRAFICO_ARQUITECTURAS+"}.\n"
    
    cadena+="\\begin{figure}[h]"
    cadena+="\centering\\includegraphics[width=0.85\\textwidth]{"+NOMBRE_GRAFICO_ARQUITECTURAS+"}"
    cadena+="\\caption{"+CAPTION_GRAFICO_ARQUITECTURAS+"}"
    cadena+="\\label{"+LABEL_GRAFICO_ARQUITECTURAS+"}"
    cadena+="\\end{figure}\n\n"
    
    return cadena


def rellenar_archivo_plantilla(nombre_archivo_plantilla, nombre_archivo_destino, datos):
    archivo_plantilla=open(nombre_archivo_plantilla, "r")
    lineas=archivo_plantilla.read()
    
    #Si el archivo plantilla está en formato unicode se convierte a ascii
    lineas=lineas.decode("utf-8")
    plantilla=Template(lineas, datos)
    
    archivo=open(nombre_archivo_destino, "w")
    archivo.write(str(plantilla))
    archivo.close()

def get_informe_hardware():
    datos=dict()
    
    encabezado  = get_encabezado_tabla()
    fin         = get_fin_tabla(CAPTION_TABLA_HW, NOMBRE_TABLA_HW)
    
    citas_wsn   = extraer_citas_articulos_por_tag('wsn')
    citas_mc    = extraer_citas_articulos_por_tag('mc')
    
    #Se recodifican ambas listas de citas
    citas_wsn   = citas_wsn.encode("utf-8")
    citas_mc    = citas_mc.encode("utf-8")
    
    caja_wsn= construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, citas_wsn)
    caja_mc = construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, citas_mc)
    
    fila_wsn="Redes de sensores"+" &"+caja_wsn+"\\\\\n\\hline\n"
    fila_mc ="Microcontroladores"+" &"+caja_mc+"\\\\\n\\hline\n"
    #Se rellena la tabla
    datos["tabla"] = encabezado + fila_wsn + fila_mc + fin
    etiquetas=['wsn', 'mc']
    textos=["WSN", "Microcontroladores"]
    #Se obtienen algunos resultados
    cantidades=extraer_vector_cantidades(etiquetas)
    
    
    resultados_wsn  = cantidades[0]
    resultados_mc   = cantidades[1]
    
    datos["total_por_wsn"]  =   resultados_wsn
    datos["total_por_mc"]   =   resultados_mc
    datos["total_por_hw"]   =   resultados_wsn + resultados_mc
    
    #Referencia de la tabla para citar como \ref
    datos["nombre_tabla"]       =   NOMBRE_TABLA_HW
    
    #Para el gráfico
    datos["NOMBRE_GRAFICO_HW"]  =   NOMBRE_GRAFICO_HW
    datos["LABEL_GRAFICO_HW"]   =   LABEL_GRAFICO_HW
    
    datos["CAPTION_GRAFICO_HW"] =   CAPTION_GRAFICO_HW
    #Finalmente se quitó el caption en los gráficos, no quedaba muy bien
    #generar_pie(textos, cantidades,  CAPTION_TABLA_HW.decode("utf-8"), NOMBRE_GRAFICO_HW )
    generar_pie(textos, cantidades,  "", NOMBRE_GRAFICO_HW )
    rellenar_archivo_plantilla(PLANTILLA_HW_TMPL,PLANTILLA_HW_TEX, datos)
    
def get_tipo_maquina():
    datos=dict()
    
    encabezado  = get_encabezado_tabla()
    fin         = get_fin_tabla(CAPTION_TABLA_TIPO, NOMBRE_TABLA_TIPO)
    
    citas_pila   = extraer_citas_articulos_por_tag('pila')
    citas_reg    = extraer_citas_articulos_por_tag('reg')
    
    #Se recodifican ambas listas de citas
    citas_pila   = citas_pila.encode("utf-8")
    citas_reg    = citas_reg.encode("utf-8")
    
    caja_pila= construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, citas_pila)
    caja_reg = construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, citas_reg)
    
    fila_pila="Basadas en pila"+" &"+caja_pila+"\\\\\n\\hline\n"
    fila_reg ="Basadas en registros"+" &"+caja_reg+"\\\\\n\\hline\n"
    #Se rellena la tabla
    datos["tabla"] = encabezado + fila_pila + fila_reg + fin
    etiquetas=['pila', 'reg']
    textos=["Pila", "Registros"]
    #Se obtienen algunos resultados
    cantidades=extraer_vector_cantidades(etiquetas)
    
    
    resultados_pila = cantidades[0]
    resultados_reg  = cantidades[1]
    
    datos["total_por_pila"]  =   resultados_pila
    datos["total_por_reg"]   =   resultados_reg
    datos["total_por_tipo"]  =   resultados_pila + resultados_reg
    
    #Referencia de la tabla para citar como \ref
    datos["nombre_tabla"]       =   NOMBRE_TABLA_TIPO
    
    #Para el gráfico
    datos["NOMBRE_GRAFICO_TIPO"]  =   NOMBRE_GRAFICO_TIPO
    datos["LABEL_GRAFICO_TIPO"]   =   LABEL_GRAFICO_TIPO
    
    datos["CAPTION_GRAFICO_TIPO"] =   CAPTION_GRAFICO_TIPO
    #Finalmente se quitó el caption en los gráficos, no quedaba muy bien
    #generar_pie(textos, cantidades,  CAPTION_TABLA_TIPO.decode("utf-8"), NOMBRE_GRAFICO_HW )
    generar_pie(textos, cantidades,  "", NOMBRE_GRAFICO_TIPO )
    rellenar_archivo_plantilla(PLANTILLA_TIPO_TMPL,PLANTILLA_TIPO_TEX, datos)



"""Devuelve un parbox con citas sobre los articulos que se refieren a
máquinas virtuales con tamaños comprendidos entre tam1 y tam2"""
def extraer_fila_tamanos(tam1, tam2):
    consulta='select distinct id_articulos.id_articulo, tam \
             from id_articulos, articulo_tiene_tag where \
                id_articulos.id_articulo=articulo_tiene_tag.id_articulo and \
                id_articulos.tam> %(tam1)d and id_articulos.tam< %(tam2)d'
    sql=consulta % {"tam1":tam1, "tam2":tam2}
    filas=ejecutar_sql(sql)
    citas=[]
    for f in filas:
        citas.append(f[0])
    lista=crear_lista_separada(citas, ", ", "\cite{", "}")
    caja=construye_parbox(ALINEAMIENTO_TABLAS, ANCHURA_CM_TABLAS, lista)
    return caja

def get_tamanos():
    datos=dict()
    sql_cantidad_maquinas_con_tam_conocido="select count(*) from id_articulos \
                                       where id_articulos.tam>0"
    cantidad_maquinas_con_tam_conocido=extraer_valor_unico_sql(
        sql_cantidad_maquinas_con_tam_conocido
    )
    
    sql_maq_mas_grande="select id_articulo,tam from id_articulos \
                       group by(id_articulo) \
                       having tam=(select max(tam) from id_articulos)"
    sql_maq_mas_peq="select id_articulo,tam from id_articulos \
                       group by(id_articulo) \
                       having tam=(select min(tam) from id_articulos\
                       where tam>0)"
    maquina_mayor=ejecutar_sql ( sql_maq_mas_grande )
    maquina_menor=ejecutar_sql ( sql_maq_mas_peq    )
    
    datos["maq_mayor"]= maquina_mayor[0][0]
    datos["tam_mayor"]= maquina_mayor[0][1]
    datos["nombre_mayor"]=datos["maq_mayor"].upper
    
    datos["maq_mas_peq"]= maquina_menor[0][0]
    datos["tam_mas_peq"]= maquina_menor[0][1]
    datos["nombre_menor"]=datos["maq_mas_peq"].upper
    
    consulta='select distinct id_articulos.id_articulo, tam \
             from id_articulos, articulo_tiene_tag where \
                id_articulos.id_articulo=articulo_tiene_tag.id_articulo and \
                id_articulos.tam> %(tam1)d and id_articulos.tam< %(tam2)d'
    
    texto=""
    textos=[]
    textos.append(u"Tamaño menor de 16KB")
    textos.append(u"Tamaño de entre 16 y 32 KB")
    textos.append(u"Tamaños mayores de 32 KB")
    
    #Esta caja contiene la lista de citas de maquinas pequeñas (<16KB)
    caja_peq=extraer_fila_tamanos(0, 16384)
    #Esto es para saber cuantas VMs hay de cada tipo
    sql=consulta % {"tam1":0, "tam2": 16384}
    tamanos=[]
    resultado = ejecutar_sql(sql)
    #La posicion 0 es el codigo de articulo, y el 1 el tamaño
    tamanos.append ( len (resultado)  )
    
    #Esta caja contiene la lista de citas de maquinas medianas(16KB a 32)
    caja_med=extraer_fila_tamanos(16384, 32768)
    
    #Esto es para saber cuantas VMs hay de cada tipo
    sql=consulta % {"tam1":16384, "tam2": 32768}
    resultado = ejecutar_sql(sql)
    #La posicion 0 es el codigo de articulo, y el 1 el tamaño
    tamanos.append ( len (resultado)  )
    
    #Esta caja contiene la lista de citas de maquinas grandes(>32Kb)
    caja_grande=extraer_fila_tamanos(32768, 500000)
    
    #Esto es para saber cuantas VMs hay de cada tipo
    sql=consulta % {"tam1":32768, "tam2": 500000}
    resultado = ejecutar_sql(sql)
    #La posicion 0 es el codigo de articulo, y el 1 el tamaño
    tamanos.append ( len (resultado)  )
    
    
    tabla=unicode(get_encabezado_tabla())
    tabla+=textos[0]+" &" + caja_peq      +   "\\\\\n\\hline\n"
    tabla+=textos[1]+" &" + caja_med      +   "\\\\\n\\hline\n"
    tabla+=textos[2]+" &" + caja_grande   +   "\\\\\n\\hline\n"
    tabla+=get_fin_tabla(CAPTION_TABLA_ESTADISTICAS, NOMBRE_TABLA_ESTADISTICAS)
    texto+=tabla
    datos["texto"]=tabla
    print tamanos
    generar_grafico_barras(tamanos, NOMBRE_GRAFICO_ESTADISTICAS)
    rellenar_archivo_plantilla(
        PLANTILLA_ESTADISTICAS_TMPL,PLANTILLA_ESTADISTICAS_TEX, datos)
    
    

get_tamanos()

etiquetas=["so", "vmjava", "vmnojava"]
conceptos=["Sistema operativo", "VM Java", "VM no Java"]

informe=get_informe_arquitectura(etiquetas, conceptos)
tabla=get_tabla_por_arquitectura()
archivo_arquitectura=open("por_arquitectura.tex", "w")
for i in range(0,len(informe)):
    #print "i:"+str(i)+"-->"+informe[i]
    pass
archivo_arquitectura.write(informe)
archivo_arquitectura.write(tabla)
archivo_arquitectura.close()

cantidades=extraer_vector_cantidades(etiquetas)
generar_pie(conceptos, cantidades, "", NOMBRE_GRAFICO_ARQUITECTURAS)



get_informe_hardware()
get_tipo_maquina()
