\chapter{Introducción}

En la actualidad, el uso de microprocesadores se ha multiplicado hasta alcanzar todas las áreas de la vida diaria. La disminución de sus costes y el aumento de sus prestaciones ha facilitado esta expansión.

Por otro lado las máquinas virtuales ofrecen diversas ventajas a los programadores, entendiendo como máquina virtual al software que simula una arquitectura (real o ficticia) y que permite ejecutar programas como si se ejecutaran en una máquina real. En este trabajo no se hará referencia a los programas de virtualización que simulan hardware real, tales como los conocidos VirtualBox o VMWare, que suelen denominarse <<monitores>>, sino que se abordará el estudio de los <<intérpretes de bytecodes>>.

Tradicionalmente, el diseño de los microprocesadores se ha analizado desde dos perspectivas diferentes~\cite{librostallings}:
\begin{itemize}
\item{Se habla de la {\em organización} cuando se estudian aspectos como el diseño interno de los mismos o su tecnología de fabricación. Es decir, cosas que el programador del dispositivo no va a ver de forma directa.}
\item{Se habla de la {\em arquitectura} cuando se estudian el número de registros disponibles, el ancho de palabra, los flags disponibles y por supuesto {\em el juego de instrucciones disponible}, cosas que los programadores si utilizarán directamente.}
\end{itemize}

Los dispositivos empotrados pueden ser de diversos tipos:

\begin{itemize}
\item{Por un lado, se denomina {\em microcontrolador} a hardware que en muchas ocasiones suelen ser los de menor velocidad de una familia de microprocesadores, pero que aún así suelen ser más potentes que los demás que aquí se enumeran. Se utilizan a menudo en fábricas y entornos similares para el control de otros dispositivos como componentes de una cadena de montaje. En este segmento pueden incluirse los procesadores ARM, muchos de Motorola (el modelo 68HC11 es uno de los más utilizados por la industria), e incluso algunos dispositivos que aún siendo antiguos continúan en uso, como los Zilog Z-80.}
\item{Nodos de redes de sensores (WSN): se caracterizan por tener menos potencia que los anteriores ya que se diseñan con propósitos distintos, tales como ser dispersados en entornos exteriores sin posibilidad de obtener alimentación externa, por lo que necesitan consumir muy poco y disponer de mecanismos de comunicación inalámbrica. Dentro de este grupo pueden incluirse dispositivos como los de la familia Mica o los Telos.}
\item{Otros componentes: por ejemplo, las FPGA son dispositivos fácilmente programables que pueden programarse para realizar cualquier tarea sin necesidad de control adicional. Suelen utilizarse de forma similar a los microprocesadores con la diferencia de resultar más genéricos.}
\end{itemize}

En este sentido, una máquina virtual podría estudiarse desde ambos puntos de vista, siendo en este caso el segundo el que será el objeto de estudio de este trabajo. En concreto se desea examinar el diseño de juegos de instrucciones en máquinas virtuales destinadas a ejecutarse en dispositivos empotrados, como por ejemplo microcontroladores.

En este trabajo pretende analizarse una característica arquitectural muy
visible: los juegos de instrucciones. En concreto va a hacerse un estudio de las
características arquitectónicas de las máquinas existentes y se va a analizar la
posibilidad de diseñar y evaluar de manera automática los juegos de
instrucciones en máquinas virtuales.

Dado que incluso un procesador virtual reducido puede mostrar muchas posibles
combinaciones en las operaciones está claro que el problema aquí analizado puede
ser muy difícil de resolver desde el punto de vista temporal. Por ello, se va a
plantear la posibilidad de utilizar técnicas de {\em soft-computing} que sin
hacer una exploración intensiva de todo el espacio de búsqueda, sí puede
permitir alcanzar soluciones con buenos resultados, pero en un lapso de tiempo
asumible.

Usadas con éxito en diversos problemas, algunas de las técnicas de soft-computing más conocidas serían:

\begin{itemize}
\item Redes neuronales: utilizando un modelo matemático de las neuronas, estas
  redes interconectan diversos nodos que simulan de forma aproximada el
  funcionamiento del cerebro. Se han utilizado a menudo para resolver problemas
  de aprendizaje.

\item Redes bayesianas: aplicadas también a problemas de aprendizaje, las redes
  bayesianas conectan nodos cuyos arcos tienen valores de probabilidad y que en
  conjunto pueden responder a preguntas del tipo {\em ¿cual es la probabilidad
    de que se dé un cierto suceso?}.

\item Sistemas expertos: estos sistemas disponen de bases de datos de hechos y
  relaciones entre ellos que pueden utilizar para descubrir nuevo conocimiento
  que de otra forma hubiera sido muy difícil (o costoso) de descubrir.

\end{itemize}

En este trabajo se defiende la posibilidad de aplicar algoritmos genéticos a
la generación de juegos de instrucciones en máquinas virtuales. A grandes
rasgos, este trabajo se divide en las siguientes partes:


\begin{enumerate}
\item Estudio pormenorizado de las distintas máquinas virtuales existentes para
    microcontroladores y dispositivos empotrados: se clasifican las distintas
    máquinas y se analizan las características más interesantes de cada una de
    ellas.
\item Se estudian las principales características de los métodos más modernos
    aplicados al diseño de juegos de instrucciones.
\item Partiendo de estos dos puntos se construye una propuesta de trabajo de
    investigación analizando los problemas que podrían surgir durante su
    realización.
\item Se muestran algunos resultados preliminares de un software desarrollado
    para la simulación de máquinas virtuales genéricas.
\item Finalmente se exponen las conclusiones a las que se han llegado tras estas
    fases de estudio y desarrollo.
\item A modo de anexo, se detallan los contenidos adquiridos durante la fase de
    formación de este programa de Máster.
\end{enumerate}
